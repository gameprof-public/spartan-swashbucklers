using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(TMP_Text))]
public class UISlider_Speed : MonoBehaviour {
    private TMP_Text txt;
    
    public void SpeedSliderValueChanged(float value) {
        if ( txt == null ) {
            txt = GetComponent<TMP_Text>();
        }
        txt.text = $"{value}x";
        GameManager.SETTINGS?.SetTimeScale( value, this );
    }
}
