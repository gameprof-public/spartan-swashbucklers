using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

/// <summary>
/// I turned SenseInfo into a class so that references (rather than copies) would be passed to you. - JGB 2023-04-08
/// If any value is -1, then it is invalid.
/// </summary>
public class SenseInfo {
    public readonly bool       valid = false;
    public readonly int        mapDistance = -1;
    public readonly Vector2Int mapLoc = new Vector2Int(-1, -1);
    public readonly eMapSense  senseType = eMapSense.none; // Includes port, port type, battle, ship, whether ship is pirate or merchant
    public readonly Vector3    position = Vector3.negativeInfinity;
    public readonly eShipClass shipClass = eShipClass.none;
    public readonly int        shipID = -1;
    public readonly bool       sailsUp = false;
    public readonly int        health = -1,  crew = -1, cannon_loaded = -1;
    public readonly float      heading = -1, speed = -1, wealth = -1;
    public readonly string     captain = "";
    
    // New function for default, invalid SenseInfo
    public SenseInfo() {}

    // New function called by Ships
    public SenseInfo( int dist, Ship ship ) {
        eMapSense filter = GameManager.SETTINGS.GetMapSenseDistanceFilter( dist );

        mapDistance = dist;
        
        // // Default values
        // valid = false;
        // mapLoc = new Vector2Int( -1, -1 );
        // senseType = eMapSense.none;
        // position = Vector3.negativeInfinity;
        // shipClass = eShipClass.none;
        // shipID = -1; // -1 is an invalid shipID
        // sailsUp = false;
        // health = crew = cannon_loaded = -1;
        // heading = speed = -1;
        // captain = "";
        
        // If there is nothing sensed or no ship, an invalid SenseInfo is returned
        if ( filter == eMapSense.none || ship == null ) { return; }
        
        valid = true; // From here on, it's actually making a valid SenseInfo
        mapLoc = ship.mapLoc;
        
        senseType = eMapSense.none;
        if ( filter.HasFlag( eMapSense.battle ) ) {
            if ( ship.health < ship.sst.healthMax / 2 ) senseType |= eMapSense.battle;
        }

        if ( filter.HasFlag( eMapSense.ship ) ) senseType |= eMapSense.ship;

        if ( filter.HasFlag( eMapSense.ship_id ) ) {
            senseType |= eMapSense.ship_id;
            shipID = ship.shipID;
        }

        if ( filter.HasFlag( eMapSense.sails_up ) ) {
            senseType |= eMapSense.sails_up;
            sailsUp = ship.GetSailsUp();
        }

        if ( ship.shipClass == eShipClass.swashbuckler ) {
            if ( filter.HasFlag( eMapSense.ship_pirate ) ) senseType |= eMapSense.ship_pirate;
        } else {
            if ( filter.HasFlag( eMapSense.ship_merchant ) ) senseType |= eMapSense.ship_merchant;
        }

        if ( filter.HasFlag( eMapSense.ship_class ) ) {
            senseType |= eMapSense.ship_class;
            shipClass = ship.shipClass;
        }

        if ( filter.HasFlag( eMapSense.heading ) ) {
            senseType |= eMapSense.heading;
            heading = ship.heading;
        }
        if ( filter.HasFlag( eMapSense.speed ) ) {
            senseType |= eMapSense.speed;
            speed = ship.speed;
        }
        if ( filter.HasFlag( eMapSense.position ) ) {
            senseType |= eMapSense.position;
            position = ship.transform.position;
        }
        if ( filter.HasFlag( eMapSense.health ) ) {
            senseType |= eMapSense.health;
            health = (int) ship.health;
        }
        if ( filter.HasFlag( eMapSense.crew ) ) {
            senseType |= eMapSense.crew;
            crew = (int) ship.crew;
        }
        if ( filter.HasFlag( eMapSense.cannon_loaded ) ) {
            senseType |= eMapSense.cannon_loaded;
            cannon_loaded = (int) ship.cannon;
        }
        if ( filter.HasFlag( eMapSense.captain ) ) {
            senseType |= eMapSense.captain;
            if ( ship.captainSO != null ) captain = ship.captainSO.captainName;
        }
        wealth = ship.loot;
        // if ( filter.HasFlag( eMapSense.wealth ) ) {
        //     senseType |= eMapSense.wealth;
        //     wealth = ship.loot;
        // }
        //TODO: Fix the filter creation in the SETTINGS Scriptable Object
    }

    public SenseInfo( int dist, eMapSense portType, Vector2Int tMapLoc ) {
        eMapSense filter = GameManager.SETTINGS.GetMapSenseDistanceFilter( dist );

        mapDistance = dist;
        
        // // Default values
        // valid = false;
        // mapLoc = new Vector2Int( -1, -1 );
        // senseType = eMapSense.none;
        // position = Vector3.negativeInfinity;
        // shipClass = eShipClass.none;
        // shipID = -1; // -1 is an invalid shipID
        // sailsUp = false;
        // health = crew = cannon_loaded = -1;
        // heading = speed = -1;
        // captain = "";
        
        // If there is nothing sensed or no port, an invalid SenseInfo is returned
        if ( filter == eMapSense.none || portType == eMapSense.none ) { return; }

        valid = true; // From here on, it's actually making a valid SenseInfo
        mapLoc = tMapLoc;
        senseType = eMapSense.none;
        if ( filter.HasFlag( eMapSense.port ) ) {
            senseType |= eMapSense.port;
        }
        if ( filter.HasFlag( portType ) ) {
            senseType |= portType;
        }
    }
    
    public override string ToString() {
        System.Text.StringBuilder sb = new StringBuilder();
        sb.AppendLine( $"valid: {valid}" );
        sb.AppendLine( $"senseType: {senseType}" );
        sb.AppendLine( $"mapLoc: {mapLoc}" );
        sb.AppendLine( $"position: {position}" );
        sb.AppendLine( $"shipClass: {shipClass}" );
        sb.AppendLine( $"shipID: {shipID}" );
        sb.AppendLine( $"captain: {captain}" );
        sb.AppendLine( $"sailsUp: {sailsUp}" );
        sb.AppendLine( $"health: {health}" );
        sb.AppendLine( $"crew: {crew}" );
        sb.AppendLine( $"cannon_loaded: {cannon_loaded}" );
        sb.AppendLine( $"heading: {heading}" );
        sb.AppendLine( $"speed: {speed}" );
        return sb.ToString();
    }
}
