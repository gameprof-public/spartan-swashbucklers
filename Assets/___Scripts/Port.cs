using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SenseInfoReporter))]
public class Port : MonoBehaviour {
    public eMapSense portType = eMapSense.port;
}
