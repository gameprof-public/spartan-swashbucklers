using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XnTools;

public class MerchantManager : MonoBehaviour {
    static private MerchantManager _S;
    
    static private Dictionary<int, Ship> ACTIVE_MERCHANT_SHIPS = new Dictionary<int, Ship>();

    static private Dictionary<eShipClass, List<int>> ACTIVE_SHIPS_BY_TYPE;
    static private Dictionary<eShipClass, float>     LAST_SPAWN_BY_TYPE;
    static private List<Vector2Int>                  SETTLEMENTS;

    static public Transform __SHIPS__Transform;

    public Transform __Ships__Parent;
    
    private Vector2Int lastSettlementMapLoc = Vector2Int.zero;

    void Awake() {
        if ( _S != null ) {
            Debug.LogError("Like the Highlander, there can be only one SeaMap!");
            Destroy( gameObject );
            return;
        }
        _S = this;
        
        ACTIVE_MERCHANT_SHIPS = new Dictionary<int, Ship>();
        ACTIVE_SHIPS_BY_TYPE = new Dictionary<eShipClass, List<int>>();
        LAST_SPAWN_BY_TYPE = new Dictionary<eShipClass, float>();

        __SHIPS__Transform = __Ships__Parent;
    }
    
    void FixedUpdate() {
        LaunchShips();
    }

    void LaunchShips() {
        foreach ( MerchantSpawnRate mSR in GameManager.SETTINGS.merchantSpawnRates ) {
            // Make sure the type is in there
            if ( !ACTIVE_SHIPS_BY_TYPE.ContainsKey( mSR.shipClass ) ) {
                ACTIVE_SHIPS_BY_TYPE.Add( mSR.shipClass, new List<int>() );
            }
            if ( !LAST_SPAWN_BY_TYPE.ContainsKey( mSR.shipClass ) ) {
                LAST_SPAWN_BY_TYPE.Add( mSR.shipClass, -1 );
            }
            
            // Spawn a ship if necessary
            if ( Time.time - LAST_SPAWN_BY_TYPE[mSR.shipClass] >= mSR.secondsBetweenSpawn ) {
                if ( ACTIVE_SHIPS_BY_TYPE[mSR.shipClass].Count < mSR.maxNumAllowed ) {
                    SpawnShip( mSR );
                }
            }
        }
    }

    void SpawnShip( MerchantSpawnRate mSR ) {
        GameObject shipPrefab = GameManager.SETTINGS.shipSettingsDict[mSR.shipClass].prefab;
        GameObject shipGO = Instantiate<GameObject>( shipPrefab, __SHIPS__Transform );
        Ship ship = shipGO.GetComponent<Ship>();
        shipGO.name = $"M{ship.shipID}-{mSR.shipClassString}";
        // Pick a random settlement to start at, just make sure it's not the same one as last time.
        Vector2Int mapLoc = SETTLEMENTS[ SETTLEMENTS.RandomIndex() ];
        if (mapLoc == lastSettlementMapLoc) mapLoc = SETTLEMENTS[ SETTLEMENTS.RandomIndex() ];
        lastSettlementMapLoc = mapLoc;
        Vector3 randomOffset = Random.insideUnitSphere * (SeaMap.TERRAIN_SCALE * 0.5f);
        randomOffset.y = 0;
        shipGO.transform.position = SeaMap.Position( mapLoc ) + randomOffset;
        shipGO.transform.rotation = Quaternion.Euler( 0, Random.Range( 0, 360 ), 0 );
        ship.captainSO = mSR.merchantCaptain.Clone();
        ( ship.captainSO as iMerchant )?.AssignSettlements( SETTLEMENTS );
        // Modify all the lists
        ACTIVE_SHIPS_BY_TYPE[mSR.shipClass].Add( ship.shipID );
        LAST_SPAWN_BY_TYPE[mSR.shipClass] = Time.time;
        ACTIVE_MERCHANT_SHIPS.Add( ship.shipID, ship );
    }

    static public void ShipArrived( SenseInfo shipInfo ) {
        if ( !ACTIVE_MERCHANT_SHIPS.ContainsKey( shipInfo.shipID ) ) {
            Debug.LogError($"Merchant ship attempted to arrive but was not an active ship: {shipInfo.shipClass}"
            + $"\n\n{shipInfo}");
            return;
        }
        Ship ship = ACTIVE_MERCHANT_SHIPS[ shipInfo.shipID ];
        RecycleMerchantShip( ship );
    }

    static public void ShipSank( SenseInfo shipInfo ) {
        // ShipArrived( shipInfo );
        if ( !ACTIVE_MERCHANT_SHIPS.ContainsKey( shipInfo.shipID ) ) {
            Debug.LogError($"Merchant ship attempted to SINK but was not an active ship: {shipInfo.shipClass}"
                           + $"\n\n{shipInfo}");
            return;
        }
        Ship ship = ACTIVE_MERCHANT_SHIPS[ shipInfo.shipID ];
        RecycleMerchantShip( ship );
    }
    

    static public void ShipSank( Ship ship ) {
        if ( !ACTIVE_MERCHANT_SHIPS.ContainsKey( ship.shipID ) ) {
            Debug.LogError($"Merchant ship sank but was not an active ship: {ship.shipClass}"
                           + $"\n\n{ship}");
            return;
        }
        RecycleMerchantShip( ship );
    }

    static public void RecycleMerchantShip( Ship ship ) {
        ACTIVE_MERCHANT_SHIPS.Remove( ship.shipID );
        Destroy( ship.gameObject );
        ACTIVE_SHIPS_BY_TYPE[ship.shipClass].Remove( ship.shipID );
    }


    static public void SET_SETTLEMENTS( List<Vector2Int> settlementsList ) {
        SETTLEMENTS = settlementsList;
    }
}

public interface iMerchant {
    public void AssignSettlements( List<Vector2Int> tList );
}
