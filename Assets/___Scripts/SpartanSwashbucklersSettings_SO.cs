using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using XnTools;
#if UNITY_EDITOR
using UnityEditor;
#endif


#region PUBLIC_REGION - You may use anything in here, though of course you can't edit anything
[CreateAssetMenu( fileName = "Spartan Swashbucklers Settings",
    menuName = "ScriptableObjects/Spartan Swashbucklers Settings", order = 1 )]
public class SpartanSwashbucklersSettings_SO : ScriptableObject {
    [Range( 0, 20 )][OnValueChanged("UpdateTimeScale")][SerializeField]
    private float timeScale = 1;
    private void UpdateTimeScale() { Time.timeScale = timeScale; }
    public float GetTimeScale() => timeScale;

    internal void SetTimeScale( float newTimeScale, object callingObject ) {
        if ( callingObject.GetType() == typeof(UISlider_Speed) ) {
            timeScale = newTimeScale;
            UpdateTimeScale();
        }
    }

    [HorizontalLine()]
    [ShowAssetPreview( 64, 64 )] 
    // TODO: Write an XnTools version that will show images larger than they actually are. - JGB 2023-03-20
    public Texture2D seaMapTexture2D;
    [Tooltip("The amount of Damage Per Second sustained by a Ship for being in a land tile")]
    public float landDPS = 100;
    
    [Range(0,100)]
    public int pirateHavenPercent = 50;
    public GameObject prefabPirateHaven, prefabSettlement;
    public float      delayBetweenHavenHeals = 30;
    
    [Space(10)][HorizontalLine(2)][Header("Captains")]
    public List<CaptainRec> captainRecs;

    [Button]
    void CheckCaptainRecsForIssues() {
        foreach ( CaptainRec rec in captainRecs ) {
            if ( rec.captainStrategySO == null ) {
                Debug.LogError( $"{rec.initials} {rec.name} is missing its CaptainStrategy_SO" );
            }
            if ( rec.flag == null ) {
                Debug.Log( $"{rec.initials} {rec.name} is missing its Flag" );
            }
            if ( !rec.spawnInGame ) {
                Debug.LogWarning( $"{rec.initials} {rec.name} is set to NOT spawn in game" );
            }
        }
    }
    
    [OnValueChanged("SortCaptainRecs")][AllowNesting]
    public bool checkThisBoxToSortCaptainRecsByName = false;
    public bool clearStatsOnStart = true;
    [OnValueChanged("SetShowStatsWhenNotPlaying")][AllowNesting]
    public bool showStatsWhenNotPlaying = false;
    public Dictionary<CaptainStrategy_SO, CaptainRec> captainRecDict;

    void SortCaptainRecs() {
        captainRecs.Sort((a,b) => String.Compare(a.name, b.name, StringComparison.InvariantCultureIgnoreCase ));
        checkThisBoxToSortCaptainRecsByName = false;
        Debug.Log("Sorted Settings.captainRecs by name."  );
    }

    void SetShowStatsWhenNotPlaying() {
        CaptainRec.showStatsWhenNotPlaying = showStatsWhenNotPlaying;
    }

    /// <summary>
    /// This method returns a Tuple containing the primary and secondary colors for a CaptainStrategy_SO
    /// </summary>
    /// <param name="csso"></param>
    /// <returns></returns>
    public (Color, Color) GetCaptainColors( CaptainStrategy_SO csso ) {
        if ( !captainRecDict.ContainsKey( csso ) ) return ( Color.white, Color.white ); // Merchant?
        CaptainRec capRec = captainRecDict[csso];
        
        // Double check the alpha values!
        if ( capRec.primaryColor.a == 0 ) capRec.primaryColor.a = 1;
        if ( capRec.secondaryColor.a == 0 ) capRec.secondaryColor.a = 1;
        
        return ( capRec.primaryColor, capRec.secondaryColor );
    }

    public Dictionary<CaptainStrategy_SO, CaptainRec> InitSwashbucklerCaptains() {
        captainRecDict = new Dictionary<CaptainStrategy_SO, CaptainRec>(captainRecs.Count);
        for ( int i = 0; i < captainRecs.Count; i++ ) {
            if ( clearStatsOnStart ) {
                captainRecs[i].moneyBanked = 0;
                captainRecs[i].killsMerchant = 0;
                captainRecs[i].killsPVP = 0;
                captainRecs[i].deathsPVP = 0;
                captainRecs[i].deathsNonPVP = 0;
            }
            if ( captainRecs[i].captainStrategySO == null ) continue;
            if ( captainRecDict.ContainsKey( captainRecs[i].captainStrategySO ) ) {
                Debug.LogError( "In Settings, captainRecs has two records with the CSSO: "         +
                                $"\nCaptain Name: {captainRecs[i].captainStrategySO.captainName} " +
                                $"\nType:{captainRecs[i].captainStrategySO.GetType()}."            +
                                "\nOnly the first was added to the captainRectDict." );
                continue;
            }
            captainRecDict.Add( captainRecs[i].captainStrategySO, captainRecs[i] );
        }
        return captainRecDict;
    }
    
    
    [Space(10)][HorizontalLine(2)][Header("Cannon")]
    public float cannonShotSpeed = 200;
    public float   cannonballDamage     = 5;
    public Vector2 cannonRangeAndHeight = new Vector2(200, 10);
    // [Tooltip("This is designed for 0.5 to be the height at which it was shot and 0 to be cannonRangeAndHeight.y/2 below that.")]
    // public AnimationCurve cannonFlightHeightCurve;
    [Tooltip("The number of degrees right or left that a cannon can aim when given an aimPoint to hit")]
    public float cannonAimDegrees = 15;
    // public float cannonMinDotForward {
    //     get { return Mathf.Cos( cannonAimDegrees * Mathf.Deg2Rad ); }
    // }
    // public float _CANNON_MAX_DOT_RIGHT {
    //     get { return Mathf.Sin( cannonAimDegrees * Mathf.Deg2Rad ); }
    // }

    private void OnValidate() {
        constMapSenseMaxDistance = MAP_SENSE_MAX_DISTANCE;
    }

    
    public const int MAP_SENSE_MAX_DISTANCE = 4;
    public const int MAP_SENSE_MAX_DISTANCE_PLUS_1 = MAP_SENSE_MAX_DISTANCE+1; // For for loops and such.
    // [Header( "Map Sense Information" )]
    [Space(10)][HorizontalLine(2)]
    [SerializeField]
    private List<MapSenseDistance> mapSenseDistances = new List<MapSenseDistance> {
        new MapSenseDistance( eMapSense.port, 4 ),
        new MapSenseDistance( eMapSense.port_haven, 3 ),
        new MapSenseDistance( eMapSense.port_settlement, 3 ),
        new MapSenseDistance( eMapSense.battle, 4 ),
        new MapSenseDistance( eMapSense.ship, 3 ),
        new MapSenseDistance( eMapSense.sails_up, 3 ),
        new MapSenseDistance( eMapSense.ship_id, 3 ),
        new MapSenseDistance( eMapSense.ship_pirate, 2 ),
        new MapSenseDistance( eMapSense.ship_merchant, 2 ),
        new MapSenseDistance( eMapSense.ship_class, 2 ),
        new MapSenseDistance( eMapSense.heading, 2 ),
        new MapSenseDistance( eMapSense.speed, 2 ),
        new MapSenseDistance( eMapSense.position, 2 ),
        new MapSenseDistance( eMapSense.captain, 1),
        new MapSenseDistance( eMapSense.health, 1 ),
        new MapSenseDistance( eMapSense.crew, 0 ),
        new MapSenseDistance( eMapSense.cannon_loaded, 0 ),
        new MapSenseDistance( eMapSense.wealth, -1 )
    };
    [SerializeField][NaughtyAttributes.ReadOnly]
    private int constMapSenseMaxDistance = MAP_SENSE_MAX_DISTANCE;

    private Dictionary<eMapSense, int> _mapSenseDistDict;
    public Dictionary<eMapSense, int> mapSenseDistDict {
        get {
            if ( _mapSenseDistDict == null ) {
                // Build the _mSSD
                _mapSenseDistDict = new Dictionary<eMapSense, int>();
                foreach ( MapSenseDistance msd in mapSenseDistances ) {
                    if ( _mapSenseDistDict.ContainsKey( msd.sense ) ) {
                        Debug.LogWarning($"Overwriting existing _mapSenseDistDict[{msd.sense.ToString()}] from" +
                                       $" {_mapSenseDistDict[msd.sense]} to {msd.distance}!"                    +
                                       $" There should NOT be two entries with the same name in mapSenseDistances!" );
                        _mapSenseDistDict[msd.sense] = msd.distance;
                    } else {
                        _mapSenseDistDict.Add( msd.sense, msd.distance );
                    }
                }
            }
            return _mapSenseDistDict;
        }
    }

    private eMapSense[] mapSenseDistanceFilters;

    eMapSense BuildMapSenseDistanceFilter( int range ) {
        eMapSense senseFilter = eMapSense.none;
        foreach ( eMapSense sense in mapSenseDistDict.Keys ) {
            if ( mapSenseDistDict[sense] == -1 ) continue;
            if ( range <= mapSenseDistDict[sense] ) {
                senseFilter |= sense;
            }
        }
        return senseFilter;
    }
    public eMapSense GetMapSenseDistanceFilter( int range ) {
        if ( range > constMapSenseMaxDistance ) return eMapSense.none;
        if ( mapSenseDistanceFilters == null || mapSenseDistanceFilters.Length < constMapSenseMaxDistance ) {
            mapSenseDistanceFilters = new eMapSense[constMapSenseMaxDistance+1];
            for ( int i = 0; i <= constMapSenseMaxDistance; i++ ) {
                mapSenseDistanceFilters[i] = BuildMapSenseDistanceFilter( i );
            }
        }
        // Below was the old Dictionary version
        // if ( !mapSenseDistanceFilters.ContainsKey( range ) ) {
        //     mapSenseDistanceFilters.Add( range, BuildMapSenseDistanceFilter( range ) );
        // }
        return mapSenseDistanceFilters[range];
    } 

    [SerializeField]
    [Space(10)][HorizontalLine(2)]
    public List<MerchantSpawnRate> merchantSpawnRates;
    

    [SerializeField]
    [Space(10)][HorizontalLine(2)]
    private List<ShipSettings> shipSettings;
    
    private Dictionary<eShipClass, ShipSettings> _shipSettingsDict;
    public Dictionary<eShipClass, ShipSettings> shipSettingsDict {
        get {
            if ( _shipSettingsDict == null ) {
                // Build the _mSSD
                _shipSettingsDict = new Dictionary<eShipClass, ShipSettings>();
                foreach ( ShipSettings sst in shipSettings ) {
                    if ( _shipSettingsDict.ContainsKey( sst.shipClass ) ) {
                        Debug.LogWarning($"Overwriting existing ShipSettings for {sst.shipClass.ToString()}!"                  +
                                         $" There should NOT be two entries with the same shipClass in shipSettings!" );
                        _shipSettingsDict[sst.shipClass] = sst;
                    } else {
                        _shipSettingsDict.Add( sst.shipClass, sst );
                    }
                }
            }
            return _shipSettingsDict;
        }
    }
}

[System.Serializable]
public class CaptainRec {
    static public bool showStatsWhenNotPlaying = false;
    
    [HideInInspector ]
    public string             name = DEFAULT_NAME;
    [OnValueChanged("UpdateName")][AllowNesting]
    public string             initials;

    public bool               spawnInGame = true;
    
    [OnValueChanged("UpdateName")][AllowNesting]
    public CaptainStrategy_SO captainStrategySO;
    public string             cSSOName => ( captainStrategySO == null ) ? "" : captainStrategySO.name;
    public string             cSSOCaptainName => ( captainStrategySO == null ) ? "" : captainStrategySO.captainName;

    [ShowAssetPreview(32,32)]
    public Texture2D          flag;

    public Color              primaryColor, secondaryColor;

    private bool isPlaying => showStatsWhenNotPlaying || Application.isPlaying;
    [ShowIf("isPlaying")][AllowNesting][TypeMismatchFix]
    public Ship               currentShip;
    [ShowIf("isPlaying")][AllowNesting]
    public int                moneyBanked;
    public int                moneyOnShip => ( currentShip == null ) ? 0 : (int) currentShip.loot;
    public int                moneyTotal  => moneyBanked + moneyOnShip;
    [ShowIf("isPlaying")][AllowNesting]
    public int                killsMerchant, killsPVP;
    public int                killsTotal => killsMerchant + killsPVP;
    [ShowIf("isPlaying")][AllowNesting]
    public int                deathsNonPVP, deathsPVP;
    public int                deathsTotal => deathsNonPVP + deathsPVP;

    public override string ToString() {
        return $"{initials}\t$:{moneyTotal} ({moneyOnShip})\tK:{killsTotal} ({killsPVP})" +
               $"\tD:{deathsTotal} ({deathsPVP})";
    }
    public string ToStringLong() {
        return $"{initials}\t$:{moneyTotal} ({moneyOnShip})\tK:{killsTotal} ({killsPVP})" +
               $"\tD:{deathsTotal} ({deathsPVP})\t{name}\t{cSSOCaptainName}\t{cSSOName}";
    }

    void UpdateName() {
        name = ( captainStrategySO == null ) ? $"{initials} - {DEFAULT_NAME}"
            : $"{initials} - {captainStrategySO.name}";
    }

    private const string DEFAULT_NAME = "(This will be set when captainStrategySO != null)";
}

public enum eShipClass { none, swashbuckler, merchantSmall, merchantMedium, merchantLarge, merchantPirateHunter }

[System.Serializable]
public class ShipSettings {
    [Hidden] public string shipClassString;
    [OnValueChanged( "UpdateShipClassString" )][AllowNesting]
    public eShipClass shipClass;
    
    public float maxSpeed                 = 20;
    public float speedEasingPerSecond     = 0.25f;
    public float baseTurnRatePerSecond    = 60;
    public float turnBleedSpeedMultiplier = 2;
    public float healthMax                = 100;
    [Tooltip("The number of points of health that a full crew of 100 will regen per second")]
    public float healRate                 = 5;
    public float crewMax                  = 100;
    [Tooltip("The maximum number of loaded cannon pairs on this ship (for the Player ship, it's 9)")]
    public float      cannonMax                = 4;
    [Tooltip("The number of Cannon that a full crew of 100 will reload per second")]
    public float      reloadRate = 4;
    [MinMaxSlider(0,10)][AllowNesting]
    public Vector2Int minMaxLootX100 = new Vector2Int(1,2);
    public GameObject prefab;

    void UpdateShipClassString() {
        shipClassString = XnUtils.NicifyVariableName( shipClass.ToString() );
    }
}


[System.Serializable]
public class MapSenseDistance {
    public eMapSense sense;
    // [Range(-1,SpartanSwashbucklersSettings_SO.MAP_SENSE_MAX_DISTANCE)]
    public int       distance = -1;

    public MapSenseDistance( eMapSense sen, int dist = -1 ) {
        sense = sen;
        distance = dist;
    }
        
}

#if UNITY_EDITOR
[CustomPropertyDrawer( typeof( MapSenseDistance ) )]
public class MapSenseDistance_Drawer : PropertyDrawer {
    const float labelWidth = 32;
    
    

    // Draw the property inside the given rect
    public override void OnGUI( Rect position, SerializedProperty property, GUIContent label ) {
        // The following line is not necessary, but I left it here as a comment because it was difficult to find initially. - JGB 2023-03-26
        // MapSenseDistance mSD = fieldInfo.GetValue( property.serializedObject.targetObject ) as MapSenseDistance;
        int edIndent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        // if ( Event.current.type == EventType.Repaint ) {
        //     Debug.Log($"{Event.current.type} position: {position}"  );
        // }
        
        // Using BeginProperty / EndProperty on the parent property means that
        // prefab override logic works on the entire property.
        EditorGUI.BeginProperty( position, label, property );
        {
            Rect rect1 = new Rect( position );
            rect1.width = position.width * 0.45f;
            EditorGUI.PropertyField( rect1, property.FindPropertyRelative( "sense" ), GUIContent.none, false );

            rect1.x += position.width * 0.5f;
            SerializedProperty distProp = property.FindPropertyRelative( "distance" );  
            int distVal = EditorGUI.IntSlider( rect1, distProp.intValue, -1,
                SpartanSwashbucklersSettings_SO.MAP_SENSE_MAX_DISTANCE );
            if ( distProp.intValue != distVal ) distProp.intValue = distVal;
            // EditorGUI.Slider( rect1, property.FindPropertyRelative( "distance" ), -1,
            //     SpartanSwashbucklersSettings_SO.MAP_SENSE_MAX_DISTANCE );
            // EditorGUI.IntSlider( rect1, property.FindPropertyRelative( "distance" ), -1,
            //     SpartanSwashbucklersSettings_SO.MAP_SENSE_MAX_DISTANCE );
            // EditorGUI.PropertyField( rect1, property.FindPropertyRelative( "distance" ) );
        }
        EditorGUI.EndProperty();
        
        EditorGUI.indentLevel = edIndent;
    }
}

#endif

[System.Serializable]
public class MerchantSpawnRate {
    [Hidden] public string shipClassString;
    [OnValueChanged( "UpdateShipClassString" )][AllowNesting]
    public eShipClass shipClass;
    [OnValueChanged( "UpdateShipClassString" )][AllowNesting]
    public float secondsBetweenSpawn = 10;
    [NaughtyAttributes.ReadOnly][AllowNesting]
    public float spawnPerSecond;
    public int maxNumAllowed = 10;
    [AllowNesting][Expandable] public CaptainStrategy_SO merchantCaptain;
    
    
    void UpdateShipClassString() {
        shipClassString = XnUtils.NicifyVariableName( shipClass.ToString() );
        spawnPerSecond = 1f / secondsBetweenSpawn;
    }
}




[System.Flags]
public enum eMapSense {
    none = 0,
    port = 1,
    port_haven  = 1<<1,
    port_settlement = 1<<2,
    ship = 1<<3,
    ship_pirate = 1<<4,
    ship_merchant = 1<<5,
    battle = 1<<6,
    health = 1<<7,
    crew = 1<<8,
    wealth = 1<<9,
    heading = 1<<10,
    speed = 1<<11,
    speed_max = 1<<12,
    sails_up = 1<<13,
    ship_id = 1<<14,
    cannon_loaded = 1<<15,
    position = 1<<16,
    ship_class = 1<<17,
    land = 1<<18,
    sea = 1<<19,
    captain = 1<<20,
    
    port_any        = port_haven | port_settlement | port,
    port_identified = port_haven | port_settlement,
};


static public class eMapSenseExtensions {
    static public bool HasAnyFlag( this eMapSense eMS, eMapSense checkFlags ) {
        eMapSense eMSAnd = eMS & checkFlags;
        if ( eMSAnd == 0 ) {
            return false;
        } else {
            return true;
        }
    }
}
#endregion
