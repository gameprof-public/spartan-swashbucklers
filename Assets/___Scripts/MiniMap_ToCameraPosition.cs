using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class MiniMap_ToCameraPosition : MonoBehaviour {
    // private Transform camPosTransform = null;

    private bool    inited = false;
    private Vector3 _mapPos;
    private Vector3 mapPos {
        get {
            if ( !inited ) {
                RectTransform rt = GetComponent<RectTransform>();
                _mapPos = rt.position;
                _mapPos.x += MiniMap.MINIMAP_SIZE * 0.5f;
                _mapPos.y = _mapPos.y * 1080f / Screen.height - MiniMap.MINIMAP_SIZE * 0.5f;
            }
            return _mapPos;
        }
    } 
    
    
    public void Click() {
        // if ( camPosTransform == null ) {
        //     camPosTransform = SeaCamTargetGrouper.GetMapTarget();
        //     if ( camPosTransform == null ) {
        //         Debug.LogError("There is no singleton of SeaCamTargetGrouper, so clicking the map will not move the camera.");
        //         return;
        //     }
        // }
        
        Vector3 mPos = Input.mousePosition;
        mPos = TransformTo1080p( mPos );
        //mPos.x = mPos.x * 1920f / Screen.width;
        //mPos.y = mPos.y * 1080f / Screen.height;

        mPos -= mapPos;

        Vector3 worldPos = new Vector3( mPos.x, 0, mPos.y ) * MiniMap.DESCALE;
        
        
        Debug.Log( $"Clicked @ {mPos} WorldPos {worldPos}" );

        // camPosTransform.position = worldPos;
        SeaCamTargetGrouper.MoveMapFocus(worldPos);
        
    }
    
            
    Vector3 TransformTo1080p(Vector3 v) {
        v.x = v.x * 1920f / Screen.width;
        v.y = v.y * 1080f / Screen.height;
        return v;
    }
}
