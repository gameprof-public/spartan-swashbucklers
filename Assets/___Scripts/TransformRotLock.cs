using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder(5000)]
public class TransformRotLock : MonoBehaviour {
    public bool       lockX = false, lockY = false, lockZ = false;
    public Vector3Int lockValues;
    
    void LateUpdate() {
        RotLock();
    }

    private void FixedUpdate() {
        RotLock();
    }

    void RotLock() {
        Vector3 rot = transform.localEulerAngles;
        if ( lockX ) rot.x = lockValues.x;
        if ( lockY ) rot.y = lockValues.y;
        if ( lockZ ) rot.z = lockValues.z;
        transform.localEulerAngles = rot;
    }
}
