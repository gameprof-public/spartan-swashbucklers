using System;
using UnityEngine;
using System.Collections.Generic;
using System.Text;
using NaughtyAttributes;
using XnTools;
using Random = UnityEngine.Random;

[DefaultExecutionOrder(-20)]
public class SeaMap : MonoBehaviour {
    
#region PUBLIC_REGION - You may use anything in here, but don't modify anything

    public const float TERRAIN_SCALE      = 200;
    public const int   TEXTURE_RESOLUTION = 64;

    static public List<Vector2Int> MAP_PORTS { get; private set; }
    static public bool[,]          NAVIGABLE { get; private set; }
    
    [Button( "Print NAVIGABLE to Console" )]
    static void PrintNavigableToConsole() {
        if ( !Application.isPlaying ) {
            Debug.LogWarning( "The Application must be playing for NAVIGABLE to be populated." );
            return;
        }
        System.Text.StringBuilder sb = new StringBuilder();
        for ( int y = TEXTURE_RESOLUTION - 1; y > -1; y-- ) {
            for ( int x = 0; x < TEXTURE_RESOLUTION; x++ ) {
                sb.Append( NAVIGABLE[x, y] ? '_' : '0' );
            }
            sb.Append( '\n' );
        }
        Debug.LogWarning( sb.ToString() );
    }

    /// <summary>
    /// Converts a world position to map coordinates.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="z"></param>
    /// <returns></returns>
    static public Vector2Int MapLoc( float x, float z ) {
        x /= TERRAIN_SCALE;
        z /= TERRAIN_SCALE;
        // TODO: May need to add 0.5f offset here.
        x = Mathf.Round( x - 0.5f ) + HALF_TEX_RES;
        z = Mathf.Round( z - 0.5f ) + HALF_TEX_RES;
        return new Vector2Int( (int) x, (int) z );
    }
    /// <summary>
    /// Converts a world position to map coordinates.
    /// </summary>
    /// <param name="pos"></param>
    /// <returns></returns>
    static public Vector2Int MapLoc( Vector3 pos ) {
        return MapLoc( pos.x, pos.z );
    }
    /// <summary>
    /// Converts a world position to map coordinates.
    /// </summary>
    /// <param name="pos"></param>
    /// <returns></returns>
    static public Vector2Int MapLoc( Vector2 pos ) {
        return MapLoc( pos.x, pos.y );
    }

    static public int MapIndex( int x, int z ) {
        return z * TEXTURE_RESOLUTION + x;
    }
    static public int MapIndex( Vector2Int mapLoc ) {
        return mapLoc.y * TEXTURE_RESOLUTION + mapLoc.x;
    }
    static public int MapIndex( Vector3 pos ) {
        return MapIndex( Mathf.RoundToInt(pos.x), Mathf.RoundToInt(pos.z) );
    }
    static public int MapIndex( Vector2 pos ) {
        return MapIndex( Mathf.RoundToInt(pos.x), Mathf.RoundToInt(pos.y) );
    }

    static public Vector3 Position( Vector2Int mapLoc ) {
        return Position( mapLoc.x, mapLoc.y );
    }
    static public Vector3 Position( int mapX, int mapZ ) {
        Vector3 pos = Vector3.zero;
        pos.x = ( mapX - ( HALF_TEX_RES ) + 0.5f ) * TERRAIN_SCALE;
        pos.z = ( mapZ - ( HALF_TEX_RES ) + 0.5f ) * TERRAIN_SCALE;
        return pos;
    }
    static public Vector3 Position( int mapIndex ) {
        int mapX = mapIndex % TEXTURE_RESOLUTION;
        int mapZ = mapIndex / TEXTURE_RESOLUTION;
        return Position( mapX, mapZ );
    }



    static public float[] mapDistances {
        get {
            if ( _mapDistances == null ) {
                _mapDistances = new float[MAX_MAP_DIST];
                float div2;
                for ( int i = 0; i < 63; i++ ) {
                    div2 = i * 0.5f;
                    _mapDistances[i] = Mathf.Ceil( div2 ) * SQ2 + Mathf.Floor( div2 ) * SQ2;
                }
            }
            return _mapDistances;
        }
    }

    static public int GetMapDistance( float f ) {
        for ( int i = 1; i <= MAX_MAP_DIST; i++ ) {
            if ( f >= mapDistances[i - 1] ) return i-1;
        }
        return MAX_MAP_DIST + 1; // Anything over the MAX doesn't matter.
    }

    static public bool IsMapLocValid( Vector2Int mapLoc ) {
        if ( mapLoc.x < 0                   || mapLoc.y < 0 ) return false;
        if ( mapLoc.x >= TEXTURE_RESOLUTION || mapLoc.y >= TEXTURE_RESOLUTION ) return false;
        return true;
    }


    static public float DirectionToHeading( Vector3 direction ) {
        direction.y = 0;
        Quaternion rot = Quaternion.LookRotation( direction, Vector3.up );
        return rot.eulerAngles.y;
    }
    static public Vector3 HeadingToDirection( float heading ) {
        return Quaternion.Euler( 0, heading, 0 ) * Vector3.forward;
    }

    [Button]
    void DirectionAndHeadingTest() {
        System.Text.StringBuilder sb = new StringBuilder();
        sb.AppendLine( "___HeadingToDirection Tests___" );
        Vector3 dir = Vector3.forward;
        float heading;
        for ( heading = 0; heading <= 720; heading+=15 ) {
            dir = HeadingToDirection( heading );
            sb.AppendLine( $"{heading:000}\t>\tx:{dir.x:0.00}\t\tz:{dir.z:0.00}" );
        }
        
        
        sb.AppendLine( "\n\n___DirectionToHeading Tests___" );
        for ( float rad = 0; rad <= Mathf.PI*2; rad += Mathf.PI/12f ) {
            dir.x = Mathf.Sin( rad );
            dir.z = Mathf.Cos( rad ); 
            heading = DirectionToHeading( dir );
            sb.AppendLine( $"x:{dir.x:0.00}\t\tz:{dir.z:0.00}\t>\t{heading:000}" );
        }
        sb.AppendLine();
        for ( float rad = Mathf.PI*2; rad <= Mathf.PI *4; rad += Mathf.PI /12f ) {
            dir.x = Mathf.Sin( rad ) * 10;
            dir.z = Mathf.Cos( rad ) * 10; 
            heading = DirectionToHeading( dir );
            sb.AppendLine( $"x:{dir.x:0.00}\t\tz:{dir.z:0.00}\t>\t{heading:000}" );
        }
        
        Debug.LogWarning(sb);
    }
#endregion
    
    
    
#region PRIVATE_REGION - You may not use anything in here
    static private SeaMap _S;
    
    private const  int   MAX_MAP_DIST       = 75; // For a 64x64 map
    static private float SQ2                = Mathf.Sqrt( 2 );

    static private int HALF_TEX_RES;
    
    static private MapTileInfo[,]   MAP_TILE_INFO_ARRAY;
    static private List<Vector2Int> MAP_PORTS_HAVENS, MAP_PORTS_SETTLEMENTS;
    static private float[]          _mapDistances;
    static private Transform        portAnchor;
    
    
    // static private Dictionary<Vector2Int, List<SenseInfo>> SENSE_DICT;

    private Texture2D tex;
    private void Awake() {
        if ( _S != null ) {
            Debug.LogError("Like the Highlander, there can be only one SeaMap!");
            Destroy( gameObject );
            return;
        }
        _S = this;
        // Reset these System.Action events in case the game restarts
        CLEAR_SENSE_INFOS = null;
        SEA_MAP_UPDATE = null;
        
        // Get the map info from GameManager.SETTINGS
        tex = GameManager.SETTINGS.seaMapTexture2D;

        HALF_TEX_RES = TEXTURE_RESOLUTION / 2;
        
        ParseMapTileInfo();
        PublicInfo.SET_PUBLIC_SEA_MAP_INFO();
        CS_JGB_Merchant_SO.ASSIGN_SETTLEMENTS( MAP_PORTS_SETTLEMENTS );
        MerchantManager.SET_SETTLEMENTS( MAP_PORTS_SETTLEMENTS );
        GameManager.SetHavenGrid( MAP_PORTS_HAVENS );
        ShipDoctor.Init( MAP_PORTS_HAVENS );

    }


    void ParseMapTileInfo() {
        Color32 portColor = new Color32( 255, 0, 0, 128 ); 
        Color32[] pixels = tex.GetPixels32();

        MAP_TILE_INFO_ARRAY = new MapTileInfo[TEXTURE_RESOLUTION, TEXTURE_RESOLUTION];
        NAVIGABLE = new bool[TEXTURE_RESOLUTION, TEXTURE_RESOLUTION];

        // Create MapTileInfo for all pixels of the input map image
        int ndx = 0;
        MapTileInfo mTI;
        Vector2Int mapLoc;
        MAP_PORTS = new List<Vector2Int>();
        for (int v = 0; v < tex.height; v++) {
            for (int h = 0; h < tex.width; h++, ndx++) {
                mapLoc = new Vector2Int( h, v );
                mTI = new MapTileInfo( mapLoc );
                if ( pixels[ndx].a == portColor.a ) { // Port     // pixels[ndx].r == portColor.r || pixels[ndx].a == portColor.a
                    mTI.mapTileType |= eMapSense.port;
                    mTI.navigable = true;
                    MAP_PORTS.Add( mTI.mapLoc );
                } else if ( pixels[ndx].a > 128 ) { // Land
                    mTI.mapTileType |= eMapSense.land;
                    mTI.navigable = false;
                } else { // Sea
                    mTI.mapTileType |= eMapSense.sea;
                    mTI.navigable = true;
                }
                MAP_TILE_INFO_ARRAY[h, v] = mTI;
                NAVIGABLE[h, v] = mTI.navigable;
            }
        }
        
        // Assign some of the ports to be Pirate Havens
        MAP_PORTS_HAVENS = new List<Vector2Int>();
        MAP_PORTS_SETTLEMENTS = new List<Vector2Int>();
        List<Vector2Int> unassignedPorts = new List<Vector2Int>( MAP_PORTS );
        Vector2Int v2I;
        int numPirateHavens = GameManager.SETTINGS.pirateHavenPercent * unassignedPorts.Count / 100;
        for ( int i = 0; i < numPirateHavens; i++ ) {
            ndx = Random.Range( 0, unassignedPorts.Count );
            v2I = unassignedPorts[ndx];
            unassignedPorts.RemoveAt(ndx);
            MAP_TILE_INFO_ARRAY[v2I.x, v2I.y].AddPortInfo( eMapSense.port_haven );
            MAP_PORTS_HAVENS.Add( v2I );
        }
        // Make the remaining ports Settlements
        for ( int i = 0; i < unassignedPorts.Count; i++ ) {
            v2I = unassignedPorts[i];
            MAP_TILE_INFO_ARRAY[v2I.x, v2I.y].AddPortInfo( eMapSense.port_settlement );
            MAP_PORTS_SETTLEMENTS.Add( v2I );
        }
        SpawnPortPrefabs();
    }

    void SpawnPortPrefabs() {
        GameObject go;
        if ( portAnchor == null ) {
            go = new GameObject("Port Anchor");
            portAnchor = go.transform;
        }
        foreach ( Vector2Int v2I in MAP_PORTS_HAVENS ) {
            go = Instantiate<GameObject>( GameManager.SETTINGS.prefabPirateHaven, portAnchor );
            go.transform.position = Position( v2I );
        }
        foreach ( Vector2Int v2I in MAP_PORTS_SETTLEMENTS ) {
            go = Instantiate<GameObject>( GameManager.SETTINGS.prefabSettlement, portAnchor );
            go.transform.position = Position( v2I );
        }
        
        Debug.Log($"Created {MAP_PORTS_HAVENS.Count} Pirate Havens and {MAP_PORTS_SETTLEMENTS.Count} Settlements."  );
    }

    static public event System.Action SEA_MAP_UPDATE;
    private void FixedUpdate() {
        if (SEA_MAP_UPDATE     != null) SEA_MAP_UPDATE();
        if ( CLEAR_SENSE_INFOS != null ) CLEAR_SENSE_INFOS();
    }


    static public int GetShipToShipMapDistance( Ship subject, Ship observer ) {
        Vector3 delta = subject.transform.position - observer.transform.position;
        delta.y = 0;
        float dist = delta.magnitude;
        int mapDist = GetMapDistance( dist );
        return mapDist;
    }

    static public Vector2Int WRAP_MAP( Vector2Int mapLocIn, Transform trans=null ) {
        // TODO: Take heading into account so that this doesn't glitch back and forth repeatedly.
        Vector2Int mapLocOut = mapLocIn;
        if ( mapLocOut.x      < 0 ) mapLocOut.x += TEXTURE_RESOLUTION;
        else if ( mapLocOut.x >= TEXTURE_RESOLUTION ) mapLocOut.x -= TEXTURE_RESOLUTION;
        
        if ( mapLocOut.y      < 0 ) mapLocOut.y += TEXTURE_RESOLUTION;
        else if ( mapLocOut.y >= TEXTURE_RESOLUTION ) mapLocOut.y -= TEXTURE_RESOLUTION;
        
        // Move the Transform if necessary
        if ( mapLocOut != mapLocIn && trans != null) {
            Vector3 deltaPos = Position( mapLocOut ) - Position( mapLocIn );
            trans.position += deltaPos;
        }

        return mapLocOut;
    }

#region MAP_TILE_INFO_ARRAY Region - YOU MAY NOT USE THESE
    static public event System.Action CLEAR_SENSE_INFOS;
    
    static private MapTileInfo GET_MAP_TILE_INFO( Vector2Int mapLoc ) {
        if ( mapLoc.x < 0 || mapLoc.x >= TEXTURE_RESOLUTION 
             || mapLoc.y < 0 || mapLoc.y >= TEXTURE_RESOLUTION ) {
            // Map index would be out of range
            return null;
        }
        if ( MAP_TILE_INFO_ARRAY[mapLoc.x, mapLoc.y] == null ) {
            MAP_TILE_INFO_ARRAY[mapLoc.x, mapLoc.y] = new MapTileInfo( mapLoc );
        }
        return MAP_TILE_INFO_ARRAY[mapLoc.x, mapLoc.y];
    }
    
    static public void MOVE_SHIP( Ship ship, Vector2Int lastMapLoc ) {
        if ( IsMapLocValid( lastMapLoc ) ) {
            GET_MAP_TILE_INFO( lastMapLoc ).RemoveShip( ship );
        }
        if ( IsMapLocValid( ship.mapLoc ) ) {
            GET_MAP_TILE_INFO( ship.mapLoc ).AddShip( ship );
        }
    }
    
    static public void REMOVE_SHIP( Ship ship, Vector2Int mapLoc, Vector2Int lastMapLoc ) {
        if ( IsMapLocValid( lastMapLoc ) ) {
            GET_MAP_TILE_INFO( lastMapLoc ).RemoveShip( ship );
        }
        if ( mapLoc != lastMapLoc && IsMapLocValid( ship.mapLoc ) ) {
            GET_MAP_TILE_INFO( ship.mapLoc ).RemoveShip( ship );
        }
    }


    static public List<SenseInfo> GET_SENSE_INFOS_AROUND( Ship ship ) {
        Vector2Int mapLoc = ship.mapLoc;
        MapTileInfo mTI = GET_MAP_TILE_INFO( mapLoc );
        if ( mTI == null ) {
            return new List<SenseInfo>();
        }
        if ( mTI.allSenseInfosAround != null ) return mTI.allSenseInfosAround;
        // This hasn't been queried before, so it needs to be done here
        List<SenseInfo> senseInfos = new List<SenseInfo>();
        List<SenseInfo> tempListRef;
        MapTileInfo otherMTI;
        for ( int distance = 0; distance < SpartanSwashbucklersSettings_SO.MAP_SENSE_MAX_DISTANCE_PLUS_1; distance++ ) {
            foreach ( Vector2Int offset in PixelRadius.GetPixelRadius( distance ) ) {
                otherMTI = GET_MAP_TILE_INFO( mapLoc + offset );
                if ( otherMTI == null ) continue;
                tempListRef = otherMTI.GatherSenseInfosAtDistance( distance );
                if ( tempListRef == null ) continue;
                senseInfos.AddRange( tempListRef );
            }
        }
        mTI.allSenseInfosAround = senseInfos;
        return senseInfos;
    }
    
    
    
    
    // static public void ClearMTIArray() {
    //     CLEAR_SENSE_INFOS(); // Calls ClearSenseInfos on all MapTileInfo instances that are registered
    // }

    static public void AddSenseToMTIA(SenseInfoReporter sIR) {
        if ( MAP_TILE_INFO_ARRAY[sIR.mapLoc.x, sIR.mapLoc.y] == null ) {
            // Creating a new MapTileInfo registers it to be cleared.
            MAP_TILE_INFO_ARRAY[sIR.mapLoc.x, sIR.mapLoc.y] = new MapTileInfo(sIR.mapLoc);
        }
        MapTileInfo mInfo = MAP_TILE_INFO_ARRAY[sIR.mapLoc.x, sIR.mapLoc.y];
        // mInfo.AddInfo( sIR );
    }
#endregion
#endregion
}


