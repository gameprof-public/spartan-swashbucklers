using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
// using System.Text;
using UnityEngine;
using NaughtyAttributes;
using UnityEngine.Events;
using XnTools;
using XnTools.XnAI;
using Random = UnityEngine.Random;

public abstract class CaptainStrategy_XnAI_SO : CaptainStrategy_SO, iXnAI_SO {
    public const string nullNodeName = "( None Selected )";

    [HorizontalLine()][ShowIf("isNotPaused")]
    public InfoProperty aiTreeStatusInfo = new InfoProperty("Behavior Tree Status", "", false, false);
    public void UpdateAITreeStatus( string status ) {
        if (aiTreeStatusInfo == null) aiTreeStatusInfo = new InfoProperty("Behavior Tree Status", "", false, false);
        aiTreeStatusInfo.text = status;
        // aiTreeStatusInfo.showAsFoldout = true;
        // aiTreeStatusInfo.foldoutOpenByDefault = true;
    }
    
    [HideIf("isNotPaused")]
    [SerializeField][HorizontalLine()] public AITree aiTree;

    public override void PreInitCaptain() {
        base.PreInitCaptain();
        if ( this.aiTree != null ) {
            this.aiTree.SetXnAI_SO( this );
        }
    }

    protected override void OnValidate() {
        base.OnValidate();
        if ( this.aiTree != null ) {
            if ( aiTree.aiSO == null ) {
                selectedNode = null;
                this.aiTree?.SetXnAI_SO( this );
            }
            aiTree?.GenerateAITreeMethodNames( this );
        }
    }

    
#region Selected Node Buttons
    [SerializeField][HorizontalLine()]//[Header("XnAI - Selected Node Operations")]
    [BoxGroup("XnAI - Selected Node Operations")]
    protected string selectedNodeName = nullNodeName;
    protected iAINode _selectedNode;
    public iAINode selectedNode {
        get { return _selectedNode; }
        set {
            _selectedNode = value;
            if ( value != null ) {
                selectedNodeName = value.nodeName;
            } else {
                selectedNodeName = nullNodeName;
            }
        }
    }

    
#if UNITY_EDITOR
    protected bool isNotPaused => Application.isPlaying && !UnityEditor.EditorApplication.isPaused;
#else
    protected bool isNotPaused => Application.isPlaying;
#endif
    protected bool isSelectedNodeValid => (!isPlaying) && (_selectedNode != null);
    protected bool isSelector          => (isSelectedNodeValid) && ( _selectedNode.GetType() == typeof(AINode_Composite) );
    protected bool isDecorator         => (isSelectedNodeValid) && ( _selectedNode is AINode_Decorator );
    protected bool isDecoratorWithoutChild => isDecorator && (( _selectedNode as AINode_Decorator )?.childNode == null); 
    protected bool isLeaf              => (isSelectedNodeValid) && ( _selectedNode.GetType() == typeof(AINode_Leaf) );
    
    
    [ShowIf(EConditionOperator.Or, "isSelector", "isDecoratorWithoutChild")]
    [Button( "AddChild: Composite Node" )]
    protected void SelectedNode_AddComposite() {
        _selectedNode.AddComposite();
    }
    
    [ShowIf(EConditionOperator.Or, "isSelector", "isDecoratorWithoutChild")]
    [Button( "AddChild: Decorator Node" )]
    protected void SelectedNode_AddDecorator() {
        _selectedNode.AddDecorator();
    }
    
    [ShowIf(EConditionOperator.Or, "isSelector", "isDecoratorWithoutChild")]
    [Button( "AddChild: Leaf Node" )]
    protected void SelectedNode_AddLeaf() {
        _selectedNode.AddLeaf();
    }  
    
    [ShowIf( "isDecorator" )]
    [Button( "Decorator.CheckUtility" )]
    protected void CheckDecoratorUtility() {
        AINode_Decorator deco = selectedNode as AINode_Decorator;
        if ( deco == null ) {
            Debug.LogWarning($"CheckUtility: FAILED because selectedNode was NULL or not an AINode_Decorator");
            return;
        }
        string s = deco.CheckUtility().ToString();
        if ( deco.utilityDecoratorMethod[0] == iAINode.nullMethodChar ) {
            Debug.Log($"CheckUtility: <b>{deco.utilityDecoratorMethod}</b> returned <b>{s}</b>");
        } else {
            Debug.Log($"CheckUtility: <b>{deco.utilityDecoratorMethod}()</b> returned <b>{s}</b>");
        }
    }
    
        
    [ShowIf("isSelectedNodeValid")]
    [Button( "AddParent: Decorator" )]
    protected void SelectedNode_AddDecoratorParent() {
        if ( _selectedNode == null ) return;
        
        iAINode parentNode = _selectedNode.GetParent();
        if ( parentNode == null ) {
            Debug.LogError( $"You can't add a Decorator above the root-level Composite Node {_selectedNode._nodeName}." );
            return;
        }
        AINode_Decorator deco = new AINode_Decorator();
        parentNode.ReplaceChildNode( _selectedNode, deco );
        deco.AddChildNode( _selectedNode );
        
        // return true;
        //
        //
        // _selectedNode.AddDecoratorParent();
        // //selectedNode = null;
        
        // Rebuild the parent child relationships.
        aiTree.SetAITreeAndParent();
    }
  

    
    [ShowIf( "isLeaf" )]
    [Button( "Leaf.CheckUtility" )]
    public void CheckUtility() {
        AINode_Leaf leaf = selectedNode as AINode_Leaf;
        if ( leaf == null ) {
            Debug.LogWarning($"CheckUtility: FAILED because selectedNode was NULL or not an AINode_Leaf");
            return;
        }
        string s = leaf.CheckUtility().ToString();
        if ( leaf.utilityMethod == iAINode.nullMethodName ) {
            Debug.Log($"CheckUtility: <b>{iAINode.nullMethodName}</b> returned <b>{s}</b>");
        } else {
            Debug.Log($"CheckUtility: <b>{leaf.utilityMethod}()</b> returned <b>{s}</b>");
        }
    }
    
    [ShowIf( "isLeaf" )]
    [Button( "Leaf.CheckExecute" )]
    public void CheckExecute() {
        AINode_Leaf leaf = selectedNode as AINode_Leaf;
        if ( leaf == null ) {
            Debug.LogWarning($"CheckExecute: FAILED because selectedNode was NULL or not an AINode_Leaf");
            return;
        }
        List<string> statList = new List<string>();
        string s = leaf.Execute(ref statList, 0).ToString();
        if ( leaf.executeMethod == iAINode.nullMethodName ) {
            Debug.Log($"CheckExecute: <b>{iAINode.nullMethodName}</b> returned <b>{s}</b>");
        } else {
            Debug.Log($"CheckExecute: <b>{leaf.executeMethod}()</b> returned <b>{s}</b>");
        }
    }
          
    [ShowIf("isSelectedNodeValid")]
    [Button( "Delete the Selected Node" )]
    protected void SelectedNode_DeleteSelf() {
#if UNITY_EDITOR
        if ( _selectedNode.GetParent() == null ) {
            UnityEditor.EditorUtility.DisplayDialog(
                $"Delete Node ERROR",
                $"You can't delete the root-level Composite Node {_selectedNode._nodeName}.",
                "Ok" );
            return;
        }
        if ( UnityEditor.EditorUtility.DisplayDialog(
                $"Delete Node {_selectedNode._nodeName}?",
                $"Are you sure you want to delete the node {_selectedNode._nodeName}?",
                "Yes", "No!" ) ) {
            _selectedNode.DeleteSelf();
            selectedNode = null;
        }
#endif
    }
    
    
    [HideIf( "isSelectedNodeValid" )]
    [Button( "Debug Public Methods On This Captain" )]
    public void DebugPublicMethods() {
        aiTree.GenerateAITreeMethodNames(this.GetType());
        Debug.LogWarning($"<b>Utility (aFloat)</b> methods: {aiTree.utilityMethods.ToStringExpanded(true)}");
        Debug.LogWarning($"<b>Execute (eStatus)</b> methods: {aiTree.executeMethods.ToStringExpanded(true)}");
        Debug.LogWarning($"<b>UtilityDecorator (aFloat)</b> methods: {aiTree.utilityDecoratorMethods.ToStringExpanded(true)}");
    }
    
    [HideIf("isSelectedNodeValid")]
    [Button( "Clean Up Parent/Child Relationships in AITree" )]
    protected void SetAITreeAndParent() {
#if UNITY_EDITOR
        if ( UnityEditor.EditorUtility.DisplayDialog(
                "Clean Up Parent/Child Relationships",
                "You shouldn't need to use this, but if the AITree seems to be acting strangely, you can try it.",
                "Yes", "No!" ) ) {
            aiTree.SetAITreeAndParent();
        }
#endif
    }
    
    
    [HideIf("isSelectedNodeValid")]
    [Button( "Reset AITree" )]
    protected void ResetXnAI() {
#if UNITY_EDITOR
        if ( UnityEditor.EditorUtility.DisplayDialog(
                "Reset AITree",
                "Are you sure you want to delete and reset the entire AITree?",
                "Yes", "No!" ) ) {
            aiTree = new AITree( this );
            // aiTree.AddRootCompositeNode();
            selectedNode = null;
        }
#endif
    }
    
#endregion
    
    
    
#region XnAI Test Methods 
    // Test public methods
    
#region XnAI Test Methods - eStatus Execute()

    public eStatus Execute_Failed() {
        return eStatus.failed;
    }
    public eStatus Execute_Busy() => eStatus.busy;
    public eStatus Execute_Done() {
        return eStatus.success;
    }
    
#endregion
    
#region XnAI Test Methods - aFloat Utility()

    static public aFloat a100     = (aFloat) 1f;
    static public aFloat a_75     = (aFloat) 0.75f;
    static public aFloat a_50     = (aFloat) 0.5f;
    static public aFloat a_25     = (aFloat) 0.25f;
    static public aFloat a__0     = (aFloat) 0f;
    static public aFloat aDefault = (aFloat) iAINode.defaultUtility;
    
    public aFloat Utility_100() => a100;
    public aFloat Utility__75() => a_75;
    public aFloat Utility___0() => a__0;

    public aFloat Utility_Default() => aDefault;
    public aFloat Utility_Random() => Random.value;
    
#endregion
    
#region XnAI Test Methods - aFloat UtilityDecorators(aFloat u)
    /// <summary>
    /// A pass-through that does not modify u
    /// </summary>
    /// <param name="u"></param>
    /// <returns>u</returns>
    public aFloat UtilityDecorator_NoChange( aFloat u ) => u;

    // public aFloat UtilityDecorator_Always_100( aFloat u ) => a100;
    // public aFloat UtilityDecorator_Always__50( aFloat u ) => a_50;
    // public aFloat UtilityDecorator_Always___0( aFloat u ) => a__0;
    

#endregion
    
#endregion
}

