using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using XnTools;



[CreateAssetMenu( fileName = "CS_HumanPlayer_SO",
    menuName = "ScriptableObjects/Captain Strategies/Human Player", order = 1 )]
public class CS_HumanPlayer_SO : CaptainStrategy_SO {
    private const string NO_TARGETED_SHIP_STRING = "( None )";
    
    [SerializeField]
    private InfoProperty info = new InfoProperty( "Captain Strategy - Human Player",
        "<b>The key commands for play are:"         +
        "\n  - Steering: A/D or Arrow Keys"      +
        "\n  - Sails Up/Down: W/S or Arrow Keys" +
        "\n  - Fire Cannon: Space or X"          +
        "\n  - Switch Target: Tab or Z</b>", false, true );

    [Space( 10 )]
    [HorizontalLine()]
    public XnputListener xnputL;
    [HorizontalLine()]
    [SerializeField]
    private string targetedShip = NO_TARGETED_SHIP_STRING;
    [Range(0,5)] public float targetLeadTime = 1f;
    public bool logTargetingMessages = false;
    
    public override void InitCaptain() {
        xnputL = new XnputListener();
        xnputL.progressOn = eXnputProgressType.manual;
        xnputL.RegisterListeners();
        // Nothing for the Human player
    }

    public void OnDestroy() {
        xnputL.UnRegisterListeners();
    }

    protected override void SetCaptainName() {
        captainName = "Human Player";
    }

    private SenseInfo                  _selfInfo;
    private List<int>                  _shipIDs        = new List<int>();
    private Dictionary<int, SenseInfo> _shipList       = new Dictionary<int, SenseInfo>();
    private int                        _targetedShipID = -1;
    private SenseInfo                  _targetedShipInfo;

    /// <summary>
    /// <para>This function is called every FixedUpdate, and it is the only way that your Captain
    ///  can give commands to the ship. See CS_JGB_Merchant_SO and CS_HumanPlayer_SO for examples.</para>
    /// <para>Commands include:</para>
    /// <para>none - Do nothing</para>
    /// <para>sailsUp - Raise the sails to Accelerate</para>
    /// <para>sailsDown - Drop the sails to Decelerate</para>
    /// <para>turnLeft - Turn left, decreasing the heading</para>
    /// <para>turnRight - Turn right, increasing the heading</para>
    /// <para>turnToHeading, float toHeading – Turn toward toHeading</para>
    /// <para>fire - Fire all cannon pairs randomly (only the starboard or port cannon of each pair will fire)</para>
    /// <para>fireAtPoint, Vector3 aimPoint - Attempt to aim cannon at a specific point in world space. Cannon aim angle is limited by SETTINGS</para>
    /// </summary>
    public override List<Ship.Command> AIUpdate(SenseInfo selfInfo, List<SenseInfo> sensed) {
        _selfInfo = selfInfo;
        List<int> sensedIDs = new List<int>();
        for ( int i = 0; i < sensed.Count; i++ ) {
            int id = sensed[i].shipID;
            if ( id == -1 || id == _selfInfo.shipID ) continue;
            if ( !sensed[i].senseType.HasFlag( eMapSense.position ) ) continue;
            sensedIDs.Add( id );
            _shipIDs.Add( id );
            _shipList[id] = sensed[i];
        }
        List<int> removeIDs = new List<int>();
        foreach ( int id in _shipIDs ) {
            if ( !sensedIDs.Contains( id ) ) {
                removeIDs.Add( id );
            }
        }
        foreach ( int id in removeIDs ) {
            _shipIDs.Remove( id );
            _shipList.Remove( id );
        }
        // Now we're left with just the IDs of ships we can sense the position of
        int sensedIndex = sensedIDs.IndexOf( _targetedShipID );
        if ( sensedIndex == -1 ) {
            _targetedShipID = -1;
        }
        bool newTarget = false;
        if ( xnputL.GetButtonDown( Xnput.eButton.b ) || ( xnputL.GetButtonDown( Xnput.eButton.select ) ) ) {
            // Tab to the next target
            sensedIndex++;
            newTarget = true;
            if ( sensedIndex == sensedIDs.Count ) {
                sensedIndex = -1;
            }
        }

        _targetedShipID = ( sensedIndex == -1 ) ? -1 : sensedIDs[sensedIndex];
        if (_targetedShipID != -1) {
            _targetedShipInfo = _shipList[_targetedShipID];
            targetedShip = $"{_targetedShipID}: {_targetedShipInfo.captain}";
            if ( logTargetingMessages && newTarget ) 
                Debug.Log( $"{captainName} targeted Captain \"{_targetedShipInfo.captain}\" in shipID {_targetedShipID}" );
        } else {
            targetedShip = NO_TARGETED_SHIP_STRING;
            if ( logTargetingMessages && newTarget )
                Debug.Log( $"{captainName} targeted nothing" );
        }
        
        commands.Clear(); // These will also be cleared by the Ship as it attempts them.
        if ( xnputL.GetButton( Xnput.eButton.up ) ) commands.Add( new Ship.Command( Ship.eCommand.sailsUp ) );
        if ( xnputL.GetButton( Xnput.eButton.down ) ) commands.Add( new Ship.Command( Ship.eCommand.sailsDown ) );

        if (xnputL.GetButton(Xnput.eButton.left)) commands.Add( new Ship.Command( Ship.eCommand.turnLeft ) );;
        if (xnputL.GetButton(Xnput.eButton.right)) commands.Add( new Ship.Command( Ship.eCommand.turnRight ) );

        if ( xnputL.GetButton( Xnput.eButton.a ) ) {
            if ( _targetedShipID == -1 ) {
                commands.Add( new Ship.Command( Ship.eCommand.fire ) );
            } else {
                Vector3 targetDir = PublicInfo.HeadingToDirection( _targetedShipInfo.heading );
                // This would be *much* better if I took distance to target into account, but I leave that to you
                Vector3 pos = _targetedShipInfo.position + ( targetDir * ( targetLeadTime * _targetedShipInfo.speed ) );
                commands.Add( new Ship.Command( Ship.eCommand.fireAtPoint, pos ) );
            }
        }

        xnputL.Progress();
        return commands;
    }

    public override void OnDrawGizmos() {
        if ( _targetedShipID != -1 ) {
            Gizmos.color = Color.magenta;
            Vector3 offset = Vector3.up * 5;
            Gizmos.DrawLine( _selfInfo.position               +offset, _targetedShipInfo.position +offset );
            Gizmos.DrawWireSphere( _targetedShipInfo.position + offset, 20 );
        }
    }
}