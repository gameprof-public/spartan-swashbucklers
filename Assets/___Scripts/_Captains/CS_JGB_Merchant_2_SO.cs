using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using XnTools.XnAI;
using XnTools;



[CreateAssetMenu( fileName = "Captain Strategy - CS_JGB_Merchant_2_SO",
    menuName = "ScriptableObjects/Captain Strategies/CS_JGB_Merchant_2_SO", order = 1 )]
public class CS_JGB_Merchant_2_SO : CaptainStrategy_XnAI_SO, iMerchant {
    // NOTE: This is assigned by SeaMap for the Merchants, but you can populate it as you travel around - JGB 2023-04-02
    static private List<Vector2Int> _SETTLEMENTS;
    
    [HorizontalLine()]
    [SerializeField] private int   minPercentOfMapToTraverse = 30;
    [SerializeField] private bool  useDiagonalNavigation     = true;
    [SerializeField] private float navPointIsCloseEnough     = 50;
    [SerializeField] private int   varianceOnStraightLines   = 20;


    [SerializeField]
    private Vector2Int startMapLoc, endMapLoc;
    private List<Vector3> navPath;
    private AStarSeaMapPathfinder pathfinder;
    private bool                  plotNavPathCalled = false;

    public override void InitCaptain() {
        // Clear the old navigation data.
        plotNavPathCalled = false;
        navPath = new List<Vector3>();
    }

    public override void CleanUpCaptain() {
        base.CleanUpCaptain();
        // if ( _selfInfo.health <= 0 ) {
        //     MerchantManager.ShipSank( _selfInfo );
        // }
    }
    
    protected override void SetCaptainName() {
        captainName = "CS_JGB_Merchant_SO";
    }

    private SenseInfo _selfInfo;
    
    /// <summary>
    /// <para>This function is called every FixedUpdate, and it is the only way that your Captain
    ///  can give commands to the ship. See CS_JGB_Merchant_2_SO and CS_HumanPlayer_SO for examples.</para>
    /// <para>Ship.eCommands can be found in the Ship script and include:</para>
    /// <para><b>none</b> - Do nothing</para>
    /// <para><b>sailsUp</b> - Raise the sails to Accelerate</para>
    /// <para><b>sailsDown</b> - Drop the sails to Decelerate</para>
    /// <para><b>turnLeft</b> - Turn left, decreasing the heading</para>
    /// <para><b>turnRight</b> - Turn right, increasing the heading</para>
    /// <para><b>turnToHeading, float toHeading</b> – Turn toward toHeading</para>
    /// <para><b>fire</b> - Fire all cannon pairs randomly (only the starboard or port cannon of each pair will fire)</para>
    /// <para><b>fireAtPoint, Vector3 aimPoint</b> - Attempt to aim cannon at a specific point in world space. Cannon aim angle is limited by SETTINGS</para>
    /// </summary>
    public override List<Ship.Command> AIUpdate(SenseInfo selfInfo, List<SenseInfo> sensed) {
        // Ship.eCommands can be found in the Ship script and include:
        
        commands.Clear(); // These will also be cleared by the Ship as it attempts them.
        
        _selfInfo = selfInfo;

        aiTree.Execute();
        
        // Moved searching for path to FindRouteToPort

        // Moved following the path to FollowRouteToPort

        return commands;
    }

    /// <summary>
    /// Find a port that is a significant distance away and plot a path to it using A*
    /// </summary>
    void DecideWhichPortToNavTo() {
        // Choose a start and end point
        startMapLoc = _selfInfo.mapLoc;
        int iterationCount = 0;
        int minPathHCost = SeaMap.TEXTURE_RESOLUTION * minPercentOfMapToTraverse / 100;
        do {
            endMapLoc = _SETTLEMENTS[_SETTLEMENTS.RandomIndex()];
            iterationCount++;
        } while ( ( iterationCount                                                                  < 100 ) &&
                  ( AStarSeaMapPathfinder.GetHCost( startMapLoc, endMapLoc, useDiagonalNavigation ) < minPathHCost ) );
        PlotNavPath(startMapLoc, endMapLoc);
    }

    void PlotNavPath(Vector2Int mapLocFrom, Vector2Int mapLocTo) {
        pathfinder = new AStarSeaMapPathfinder();
        GameManager.START_COROUTINE(pathfinder.PathFindCoRo(mapLocFrom, mapLocTo, useDiagonalNavigation, PathFound));
        plotNavPathCalled = true;
    }

    void PathFound( List<AStarComparable> foundPath ) {
        pathfinder = null; // Release the memory for pathfinder
        if ( foundPath == null || foundPath.Count < 2 ) {
            plotNavPathCalled = false; // Ask for another path
            return;
        }
        
        float[] pathLerpAmounts = new[] { Random.Range(0.35f,0.45f), 0.5f, Random.Range(0.55f,0.65f) };
        navPath = new List<Vector3>();
        Vector3 p0, p1, n0, n1, n2, thisDir, lastDir = Vector3.zero;
        float randy = Random.Range( -varianceOnStraightLines, varianceOnStraightLines );
        for ( int i = 1; i < foundPath.Count; i++ ) {
            p0 = foundPath[i - 1].position;
            p1 = foundPath[i].position;
            n0 = Vector3.Lerp( p0, p1, pathLerpAmounts[0] );
            n1 = Vector3.Lerp( p0, p1, pathLerpAmounts[1] );
            n2 = Vector3.Lerp( p0, p1, pathLerpAmounts[2] );
            thisDir = ( n2 - n0 );//.normalized;
            if ( n0.x.IsApproximately(n1.x, 1) && n1.x.IsApproximately(n2.x,1) ) {
                // It's straight in the x dimension, so it can be varied
                n0.x += randy;
                n1.x += randy;
                n2.x += randy;
            } else if ( n0.z.IsApproximately(n1.z, 1) && n1.z.IsApproximately(n2.z,1) ) {
                // It's straight in the x dimension, so it can be varied
                n0.z += randy;
                n1.z += randy;
                n2.z += randy;
            } else if ( ( thisDir - lastDir ).magnitude < 1f ) {
                n0.x += randy;
                n1.x += randy;
                n2.x += randy;
                n0.z -= randy;
                n1.z -= randy;
                n2.z -= randy;
            }
            lastDir = thisDir;
            navPath.AddRange( n0, n1, n2 );
            // for ( int j = 0; j < pathLerpAmounts.Length; j++ ) {
            //     navPath.Add( Vector3.Lerp( p0, p1, pathLerpAmounts[j] ) );
            // }
        }
    }

    private Color col = Color.clear;
    public override void OnDrawGizmos() {
        if ( navPath != null && navPath.Count > 0 ) {
            if ( col == Color.clear ) {
                col = Random.ColorHSV( 0, 1, 1, 1, 1, 1 );
            }
            Vector3 offset = Vector3.up * 2;
            Gizmos.color = col;
            
            Gizmos.DrawLine( _selfInfo.position+offset, navPath[0]+offset );
            for ( int i = 1; i < navPath.Count; i++ ) {
                Gizmos.DrawLine( navPath[i - 1] +offset, navPath[i]+offset );
            }
            
        }
    }


    public void AssignSettlements( List<Vector2Int> tList ) {
        if (_SETTLEMENTS == null) _SETTLEMENTS = tList;
    }

    // [Button]
    // public void DebugPublicMethods() {
    //     string[] names = GetPublicMethods();
    //     System.Text.StringBuilder sb = new StringBuilder();
    //     for ( int i = 0; i < names.Length; i++ ) {
    //         sb.AppendLine( names[i] );
    //     }
    //     Debug.LogWarning( sb.ToString() );
    // }
    //
    // public string[] GetPublicMethods() {
    //     System.Reflection.MethodInfo[] methodInfos = this.GetType().GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
    //     string[] methodNames = new string[methodInfos.Length];
    //     // foreach ( System.Reflection.MemberInfo info in methodInfos ) {
    //     //     methodNames[]
    //     // }
    //     for ( int i = 0; i < methodInfos.Length; i++ ) {
    //         methodNames[i] = methodInfos[i].Name;
    //         // methodInfos[i].CustomAttributes
    //     }
    //     return methodNames;
    // }
    
#region XnAI Methods - See more example methods in CaptainStrategy_XnAI_SO.cs
    
#region XnAI Methods - Execute
    // A* Navigation to Destination Port
    public eStatus FindRouteToPort() {
        if ( navPath != null && navPath.Count > 0 ) return eStatus.success;
        if ( plotNavPathCalled ) return eStatus.busy;
        DecideWhichPortToNavTo();
        return eStatus.busy;
    }
    
    public eStatus FollowRouteToPort() {
        // Have we finished the path entirely
        if ( _selfInfo.mapLoc == endMapLoc ) {
            MerchantManager.ShipArrived( _selfInfo );
            commands.Add(new Ship.Command(Ship.eCommand.sailsDown)  );
            return eStatus.success;
        }
        
        // Determine whether we're close enough to move to the next point on the path
        Vector3 nextNavPoint = navPath[0];
        if ( ( nextNavPoint - _selfInfo.position ).magnitude <= navPointIsCloseEnough ) {
            navPath.RemoveAt( 0 );
            // Are we done?
            if ( navPath.Count == 0 ) {
                MerchantManager.ShipArrived( _selfInfo );
                commands.Add(new Ship.Command(Ship.eCommand.sailsDown)  );
                return eStatus.failed;
            }
            // Otherwise, head to the new nav point
            nextNavPoint = navPath[0];
        }

        Vector3 deltaToNavPoint = nextNavPoint - _selfInfo.position;
        float desiredHeading = SeaMap.DirectionToHeading( deltaToNavPoint );
        commands.Add( new Ship.Command( Ship.eCommand.turnToHeading, desiredHeading ) );
        if ( !_selfInfo.sailsUp ) commands.Add( new Ship.Command( Ship.eCommand.sailsUp ) );
        return eStatus.busy;
    }
    
    // Defend in Combat => Head to Nearest Port
    public eStatus IsAbleToHealInPort() {
        // Do I need to heal?
        // Has enough time passed since healing last time?
        return eStatus.failed;
    }
    
    public eStatus IsCloseEnoughToSettlement() {
        return eStatus.failed;
    }
    
    public eStatus RunToPort() {
        return eStatus.failed;
    }
    
    // Defend in Combat => Turn and Shoot
    public eStatus AreEnoughCannonLoaded() {
        return eStatus.failed;
    }
    
    public eStatus IsEnemyInRange() {
        return eStatus.failed;
    }
    
    public eStatus TurnToAim() {
        return eStatus.failed;
    }
    
    public eStatus Fire() {
        return eStatus.failed;
    }
    
    // Defend in Combat
    public eStatus RunAway() {
        return eStatus.failed;
    }
#endregion

#region XnAI Methods - Utility


    
#endregion
    
#region XnAI Methods - Utility Decorators

    

#endregion
    
#endregion
}