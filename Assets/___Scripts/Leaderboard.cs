using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using XnTools;

public class Leaderboard : MonoBehaviour {
    public const char   selectedChar = '«';
    public const string blockString  = "▁▂▃▄▅▆▇██";
    public const char   pirateChar   = '☠';
    
    
    public enum eSortType { Name, Kills, Deaths, Money }
    [SerializeField] List<CaptainRec> capts;
    List<CaptainRec>                  captsFocused;
    private TMP_Text                  tmpText;

    public int   firstIndent = 200,       tabIndent = 128;
    public Gradient faceColors;

    public Image buttonImage;

    public  eSortType sortType    = eSortType.Money;
    public  string[]  baseHeaders = new[] { "Name", "Kills", "Deaths", "$core" };
    public  string    sortPrefix  = "<u><uppercase>";
    public  string    sortPostfix = "_</uppercase></u>";
    private string    header;

    // Start is called before the first frame update
    void Awake()
    {
        // Create a sorted version of the list of CaptainRec
        capts = new List<CaptainRec>(GameManager.SETTINGS.captainRecs);
        captsFocused = new List<CaptainRec>();
        tmpText = GetComponentInChildren<TMP_Text>();

        SetSortType( sortType );
    }

    void SetSortType( eSortType newSort ) {
        sortType = newSort;
        int sortNum = (int) sortType;
        int possibleSortsCount = Enum.GetValues( typeof(eSortType) ).Length;
        
        System.Text.StringBuilder sbN = new System.Text.StringBuilder();

        for (int i = 0; i < possibleSortsCount; i++) {
            sbN.Append( $"<style=\"C{i}\">" );
            if ( i == sortNum ) {
                sbN.Append( sortPrefix );
                sbN.Append( baseHeaders[i] );
                sbN.Append( sortPostfix );
            } else {
                sbN.Append( baseHeaders[i] );
            }
            sbN.Append( $"</style>" );
            if ( i < possibleSortsCount - 1 ) {
                sbN.Append( "\t" );   
            }
        }

        header = sbN.ToString();
    }

    void FixedUpdate()
    {
        /*if (capts == null)
        {
            if (GameManager.SETTINGS == null ||
                GameManager.SETTINGS.captainRecs == null)
            {
                return;
            }

            // Create a sorted version of the list of CaptainRec
            capts = new List<CaptainRec>(GameManager.SETTINGS.captainRecs);
        }*/

        // Sort all the captains by values and name
        // This shows calling the Sort(Comparison<T>) overload using 
        //   an anonymous method for the Comparison delegate. 
        // This method sorts by value with the greater value first.
        // If value are the same, it sorts alphabetically by name
        capts.Sort(delegate (CaptainRec a, CaptainRec b)
        {
            switch (sortType)
            {
                case eSortType.Money:
                    if (a.moneyTotal < b.moneyTotal) return 1;
                    if (a.moneyTotal > b.moneyTotal) return -1;
                    break;
                case eSortType.Kills:
                    if (a.killsTotal < b.killsTotal) return 1;
                    if (a.killsTotal > b.killsTotal) return -1;
                    break;
                case eSortType.Deaths:
                    if (a.deathsTotal < b.deathsTotal) return -1;
                    if (a.deathsTotal > b.deathsTotal) return 1;
                    break;
                case eSortType.Name:
                    return a.name.CompareTo(b.name);
            }
            return a.name.CompareTo(b.name);
        });

        // Find Max/Min for moneyTotal, kills, and deaths
        Vector2 v2Money = Vector2.one, v2Kills = Vector2.one, v2Deaths = Vector2.one;
        bool gradientValsInited = false;
        for (int i = 0; i < capts.Count; i++)
        {
            if (!capts[i].spawnInGame) continue;
            
            if (!gradientValsInited)
            {
                v2Money.x = v2Money.y = capts[i].moneyTotal;
                v2Kills.x = v2Kills.y = capts[i].killsTotal;
                v2Deaths.x = v2Deaths.y = capts[i].deathsTotal;
                gradientValsInited = true;
            }
            else
            {
                v2Money.x = Mathf.Min(v2Money.x, capts[i].moneyTotal);
                v2Money.y = Mathf.Max(v2Money.y, capts[i].moneyTotal);

                v2Kills.x = Mathf.Min(v2Kills.x, capts[i].killsTotal);
                v2Kills.y = Mathf.Max(v2Kills.y, capts[i].killsTotal);

                v2Deaths.x = Mathf.Min(v2Deaths.x, capts[i].deathsTotal);
                v2Deaths.y = Mathf.Max(v2Deaths.y, capts[i].deathsTotal);
            }
        }

        int tabPos = firstIndent;
        System.Text.StringBuilder sbN = new System.Text.StringBuilder();
        sbN.AppendLine( header );
        
        // THis is all old header stuff that has been replaced - JGB 2024-04-14
        /*
        // string[] headers = { "Name", "Kil", "Dth", "$" };
        switch ( sortType ) {
        case eSortType.Name:
            sbN.AppendLine( "<u>Name</u> <style=\"C1\">Kills</style> <style=\"C2\">Deaths</style> <style=\"C3\">$core</style>" );
            // headers[0] = "<u>Name</u>";
            break;
        
        case eSortType.Kills:
            sbN.AppendLine( "Name <style=\"C1\"><u>Kills</u></style> <style=\"C2\">Deaths</style> <style=\"C3\">$core</style>" );
            // headers[0] = "<u>Kills</u>";
            break;
        
        case eSortType.Deaths:
            sbN.AppendLine( "Name <style=\"C1\">Kills</style> <style=\"C2\"><u>Deaths</u></style> <style=\"C3\">$core</style>" );
            // headers[0] = "<u>Deaths</u>";
            break;
        
        case eSortType.Money:
            sbN.AppendLine( "Name <style=\"C1\">Kills</style> <style=\"C2\">Deaths</style> <style=\"C3\"><u>$core</u></style>" );
            // headers[0] = "<u>$core</u>";
            break;
        }
        
        // sbN.Append( headers[0] );
        // for ( int i = 1; i < headers.Length; i++ ) {
        //     sbN.Append( "<pos=" + tabPos + ">"+headers[i] );
        //     tabPos += tabIndent;
        // }
        // sbN.Append( "\n" );
        
        // sbN.AppendLine("Name"); // "Name\tMon\tKil\tDth\n"
        // sbN.AppendLine("Mon\tKil\tDth");
        */
        
        float u;
        Color gray;
        foreach (CaptainRec cap in capts)
        {
            // tabPos = 0;
            // tabPos has now been replaced by a Style Sheet and <style="C_"> tags - JGB 2024-04-14

            if ( !cap.spawnInGame ) continue;

            sbN.Append( HealthCharFromCaptainRec(cap) );
            sbN.Append( $"<color={cap.primaryColor.HTML()}>" );
            sbN.Append( cap.initials.Substring( 0, 2 ) );
            sbN.Append( $"</color><color={cap.secondaryColor.HTML()}>" );
            sbN.Append( cap.initials.Substring( 2 ) );
            sbN.Append( "</color> " );
            if ( captsFocused.Contains( cap ) ) {
                sbN.Append( selectedChar );
            }

            sbN.Append( "\t<style=\"C1\">" );
            sbN.Append( $"{cap.killsTotal} <size=80%>{cap.killsPVP}<sub>PVP</sub></size>" );

            sbN.Append( "\t</style><style=\"C2\">" );
            sbN.Append( $"{cap.deathsTotal} <size=80%>{cap.deathsPVP}<sub>PVP</sub></size>" );
            
            sbN.Append( "\t</style><style=\"C3\">" );
            sbN.Append( $"${cap.moneyTotal:N0} <size=80%>${cap.moneyOnShip:N0}<sub>H</sub></size>" ); 

            sbN.AppendLine( "</style>" );
        }

        tmpText.text = sbN.ToString();
    }

    public void Click() {
        // Vector3 mPos = Input.mousePosition;
        // mPos = TransformTo1080p( mPos );
        // //mPos.x = mPos.x * 1920f / Screen.width;
        // //mPos.y = mPos.y * 1080f / Screen.height;
        // Vector3 txtPos = TransformTo1080p( tmpText.rectTransform.position );
        // mPos -= txtPos;

        // Debug.Log( "Clicked @ "+mPos );

        TMP_TextInfo textInfo = tmpText.textInfo;
        int lineNum = TMP_TextUtilities.FindIntersectingLine( tmpText, Input.mousePosition, null );
        if ( lineNum == -1 ) return;
        string line = tmpText.GetParsedText().Substring( textInfo.lineInfo[lineNum].firstCharacterIndex,
            textInfo.lineInfo[lineNum].characterCount );

        if ( lineNum == 0 ) {  // This is the header line!
            int wordNum = TMP_TextUtilities.FindIntersectingWord( tmpText, Input.mousePosition, null );
            if ( wordNum == -1 ) return;
            string word = line.Split( '\t' )[wordNum].Trim();

            // string word = tmpText.GetParsedText().Substring( textInfo.wordInfo[wordNum].firstCharacterIndex,
                // textInfo.wordInfo[wordNum].characterCount );
            for (int i = 0; i < baseHeaders.Length; i++) {
                if ( word == baseHeaders[i] ) {
                    SetSortType((eSortType) i);
                }
            }
        } else { // This is a regular captain line
            //Debug.Log( "Clicked line " + i + "\t" + txt );
            string name = line.Split( ' ' )[0][1..];
            //Debug.Log( "Clicked Name: " + name );
            foreach ( CaptainRec cap in capts ) {
                if ( name == cap.initials ) {
                    if ( Input.GetKey( KeyCode.LeftShift ) ) {
                        ToggleFocus( cap );
                    } else {
                        SoloFocus( cap );
                    }
                    // //Debug.Log( "Found Captain: " + cap.name );
                    // SeaCamera.FOCUS_SHIP( cap );
                }
            }
        }

        // for (int i=0; i<textInfo.lineInfo.Length; i++ ) {
        //     if ( textInfo.lineInfo[i].lineExtents.min.y < mPos.y
        //          && textInfo.lineInfo[i].lineExtents.max.y > mPos.y ) {
        //         string txt = tmpText.GetParsedText().Substring( textInfo.lineInfo[i].firstCharacterIndex,
        //             textInfo.lineInfo[i].characterCount );
        //         if ( i == 0 ) {  // This is the header line!
        //             int numWords = textInfo.lineInfo[i].wordCount;
        //             for (int w = 0; w < numWords; w++) {
        //                 if (textInfo.wordInfo[w].)
        //             }
        //         } else { // This is a regular captain line
        //             //Debug.Log( "Clicked line " + i + "\t" + txt );
        //             string name = txt.Split( ' ' )[0][1..];
        //             //Debug.Log( "Clicked Name: " + name );
        //             foreach ( CaptainRec cap in capts ) {
        //                 if ( name == cap.initials ) {
        //                     if ( Input.GetKey( KeyCode.LeftShift ) ) {
        //                         ToggleFocus( cap );
        //                     } else {
        //                         SoloFocus( cap );
        //                     }
        //                     // //Debug.Log( "Found Captain: " + cap.name );
        //                     // SeaCamera.FOCUS_SHIP( cap );
        //                 }
        //             }
        //         }
        //     }
        // }
    }

    void ToggleFocus( CaptainRec cap ) {
        if ( captsFocused.Contains( cap ) ) {
            captsFocused.Remove( cap );
        } else {
            captsFocused.Add( cap );
        }
        SeaCamTargetGrouper.UpdateCaptainsFocused( captsFocused );
    }
    void SoloFocus( CaptainRec cap ) {
        // Check to make sure to deselect if it's clicking the same single captain
        if ( captsFocused.Count == 1 && captsFocused[0] == cap ) {
            captsFocused.Clear();
        } else {
            captsFocused.Clear();
            captsFocused.Add( cap );
        }
        SeaCamTargetGrouper.UpdateCaptainsFocused( captsFocused );
    }

    Vector3 TransformTo1080p(Vector3 v) {
        v.x = v.x * 1920f / Screen.width;
        v.y = v.y * 1080f / Screen.height;
        return v;
    }

    char HealthCharFromCaptainRec( CaptainRec cap ) {
        if ( cap.currentShip == null ) return pirateChar;
        float healthPercent = cap.currentShip.healthPercent;
        healthPercent *= blockString.Length;
        int healthInt = Mathf.FloorToInt( healthPercent );
        healthInt = Mathf.Clamp( healthInt, 0, blockString.Length - 1 );
        return blockString[healthInt];
    }
    
    
    // string HealthToBlockString(Agent a, bool withColor=true) {
    //     float u = a.health / (float) ArenaManager.AGENT_SETTINGS.agentHealthMax;
    //     string block = blockString[Mathf.FloorToInt( u * 8 )].ToString();
    //     if (withColor) {
    //         Color c = faceColors.Evaluate( u );
    //         return "<color=" + HexConverter( c ) + ">" + block + "</color>";
    //     }
    //     return block;
    // }
    //
    // string AmmoToBlockString( Agent a, bool withColor = true ) {
    //     float u = a.ammo / (float) ArenaManager.AGENT_SETTINGS.agentAmmoMax;
    //     string block = blockString[Mathf.FloorToInt( u * 8 )].ToString();
    //     if ( withColor ) {
    //         Color c = Color.Lerp( Color.black, Color.white, u );//faceColors.Evaluate( u );
    //         return "<color=" + HexConverter( c ) + ">" + block + "</color>";
    //     }
    //     return block;
    // }
}










