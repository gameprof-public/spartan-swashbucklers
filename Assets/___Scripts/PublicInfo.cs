using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The PublicInfo class contains properties to access info that any Captain can use to make decisions.
/// NOTE: Also see the #region PUBLIC_REGION in SeaMap, which has several methods you can call.
/// </summary>
static public class PublicInfo {
#region PUBLIC_REGION - You may use anything in here, but don't modify anything
    static public SpartanSwashbucklersSettings_SO      SETTINGS            { get; private set; }
    static public Dictionary<eShipClass, ShipSettings> SHIP_SETTINGS_DICT  { get; private set; }
    static public Dictionary<eMapSense, int>           MAP_SENSE_DIST_DICT { get; private set; }
    static public List<Vector2Int>                     MAP_PORTS           { get; private set; }
    static public bool[,]                              NAVIGABLE           { get; private set; }
#endregion

    static public void SET_PUBLIC_SEA_MAP_INFO() {
        MAP_PORTS = SeaMap.MAP_PORTS;
        NAVIGABLE = SeaMap.NAVIGABLE;
    }
    
    static public void SET_PUBLIC_GAME_MANAGER_INFO() {
        SETTINGS = GameManager.SETTINGS;
        SHIP_SETTINGS_DICT = SETTINGS.shipSettingsDict;
        MAP_SENSE_DIST_DICT = SETTINGS.mapSenseDistDict;
    }
    
    static public float DirectionToHeading( Vector3 direction ) {
        direction.y = 0;
        Quaternion rot = Quaternion.LookRotation( direction, Vector3.up );
        return rot.eulerAngles.y;
    }
    static public Vector3 HeadingToDirection( float heading ) {
        return Quaternion.Euler( 0, heading, 0 ) * Vector3.forward;
    }
    
    // NOTE: Also see the #region PUBLIC_REGION in SeaMap, which has several methods you can call.
}
