using System.Collections;
using System.Collections.Generic;
using UnityEngine;

static public class PixelRadius {
    static public List<List<Vector2Int>> radiiList = null;

    static public List<Vector2Int> GetPixelRadius( int radius = 1 ) {
        if ( radiiList == null ) {
            radiiList = new List<List<Vector2Int>>();
            // Radius 0
            radiiList.Add( new List<Vector2Int>( new[] { new Vector2Int( 0, 0 ) } ) );
            
            // Radius 1
            radiiList.Add( new List<Vector2Int>( new[] {
                new Vector2Int( 1, 0 ), new Vector2Int( 1, 1 ), new Vector2Int( 0, 1 ), new Vector2Int( -1, 1 ),
                new Vector2Int( -1, 0 ), new Vector2Int( -1, -1 ), new Vector2Int( 0, -1 ), new Vector2Int( 1, -1 )
            } ) );
            
            // Radius 2
            radiiList.Add( new List<Vector2Int>( new[] {
                new Vector2Int( 2, 0 ), new Vector2Int( 2, 1 ), new Vector2Int( 1, 2 ), new Vector2Int( 0, 2 ),
                new Vector2Int( -1, 2 ), new Vector2Int( -2, -1 ), new Vector2Int( -2, 0 ), new Vector2Int( -2, -1 ),
                new Vector2Int( -1, -2 ), new Vector2Int( 0, -2 ), new Vector2Int( 1, -2 ), new Vector2Int( 2, -1 )
            } ) );
            
            // Radius 3
            radiiList.Add( new List<Vector2Int>( new[] {
                new Vector2Int( 3, 0 ), new Vector2Int( 3, 1 ), new Vector2Int( 3, 2 ), new Vector2Int( 2, 2 ),
                new Vector2Int( 2, 3 ), new Vector2Int( 1, 3 ), new Vector2Int( 0, 3 ), new Vector2Int( -1, 3 ),
                new Vector2Int( -2, 3 ), new Vector2Int( -2, 2 ), new Vector2Int( -3, 2 ), new Vector2Int( -3, 1 ),
                new Vector2Int( -3, 0 ), new Vector2Int( -3, -1 ), new Vector2Int( -3, -2 ), new Vector2Int( -2, -2 ),
                new Vector2Int( -2, -3 ), new Vector2Int( -1, -3 ), new Vector2Int( 0, -3 ), new Vector2Int( 1, -3 ),
                new Vector2Int( 2, -3 ), new Vector2Int( 2, -2 ), new Vector2Int( 3, -2 ), new Vector2Int( 3, -1 )
            } ) );
            
            // Radius 4
            radiiList.Add( new List<Vector2Int>( new[] {
                new Vector2Int( 4, 0 ), new Vector2Int( 4, 1 ), new Vector2Int( 4, 2 ), new Vector2Int( 3, 3 ),
                new Vector2Int( 2, 4 ), new Vector2Int( 1, 4 ), new Vector2Int( 0, 4 ), new Vector2Int( -1, 4 ),
                new Vector2Int( -2, 4 ), new Vector2Int( -3, 3 ), new Vector2Int( -4, 2 ), new Vector2Int( -4, 1 ),
                new Vector2Int( -4, 0 ), new Vector2Int( -4, -1 ), new Vector2Int( -4, -2 ), new Vector2Int( -3, -3 ),
                new Vector2Int( -2, -4 ), new Vector2Int( -1, -4 ), new Vector2Int( 0, -4 ), new Vector2Int( 1, -4 ),
                new Vector2Int( 2, -4 ), new Vector2Int( 3, -3 ), new Vector2Int( 4, -2 ), new Vector2Int( 4, -1 )
            } ) );

        }
        
        // Returning a list
        if ( radius >= radiiList.Count ) return null;
        return radiiList[radius];
    }
}
