using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardControls : MonoBehaviour
{
    [SerializeField]
    private InfoProperty info = new InfoProperty("Camera Controls",
        "<b>The key commands for controlling the camera are:" +
        "\n  - Pan Camera: WASD or Arrow Keys" +
        "\n  - Zoom Camera In/Out: Z/X or Comma/Period" +
        "\n  - Rotate Camera Left/Right: Q/E or Left Bracket/Right Bracket", false, true);

    [Header("Inscribed")]
    public SeaCamera seaCamera;

    private float[] zoomLerpLevels = {0.0f, 0.05f, 0.1f, 0.2f, 0.4f, 1.0f};
    private int currentZoomLerp = 0;
        
    void Update()
    {
        // Panning controls
        Vector3 deltaPos = Vector3.zero;

        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            deltaPos += Vector3.forward;
        }
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            deltaPos += Vector3.left;
        }
        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            deltaPos += Vector3.back;
        }
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            deltaPos += Vector3.right;
        }

        if (deltaPos != Vector3.zero){
            deltaPos.Normalize();
            seaCamera.ReadPanInput(deltaPos);
        }


        // Zoom controls
        // if (!seaCamera.GetEasingZoom()){

            if (Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(KeyCode.Comma)){
                if (currentZoomLerp != 0){
                    currentZoomLerp--;
                    // seaCamera.ReadZoomInput(zoomLerpLevels[currentZoomLerp]);
                    SeaCamera.EASE_TO_ZOOM_LERP( zoomLerpLevels[currentZoomLerp] );
                }
            }

            if (Input.GetKeyDown(KeyCode.X) || Input.GetKeyDown(KeyCode.Period)){
                if (currentZoomLerp != zoomLerpLevels.Length-1){
                    currentZoomLerp++;
                    // seaCamera.ReadZoomInput(zoomLerpLevels[currentZoomLerp]);
                    SeaCamera.EASE_TO_ZOOM_LERP( zoomLerpLevels[currentZoomLerp] );
                }
            }

        // }


        // Rotate Controls
        if (Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.LeftBracket)){
            seaCamera.ReadRotateInput(false);
        }
        if (Input.GetKey(KeyCode.E) || Input.GetKey(KeyCode.RightBracket)){
            seaCamera.ReadRotateInput(true);
        }
    }
}
