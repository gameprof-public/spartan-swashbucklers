using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sail : MonoBehaviour {
   [SerializeField]
   private bool _isOn, _atLevel;
   public GameObject sail;
   public GameObject onObjects, offObjects, onAtOneLevel;

   public bool isOn {
      get { return _isOn; }
      set {
         _isOn = value;
         if (onObjects  != null) onObjects.SetActive( _isOn );
         if (offObjects != null) offObjects.SetActive( !_isOn );
         if (sail != null) sail.SetActive( _isOn );
         if ( !_isOn ) atLevel = false;
      }
   }

   public bool atLevel {
      get { return _atLevel; }
      set {
         _atLevel = value;
         if (onAtOneLevel != null) onAtOneLevel.SetActive( _atLevel );
      }
   }
}
