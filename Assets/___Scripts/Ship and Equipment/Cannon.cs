using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class Cannon : MonoBehaviour {
   static private float     _CANNON_AIM_DEGREES;
   static private float     _CANNON_MIN_DOT_FORWARD;
   static private float     _CANNON_MAX_DOT_RIGHT;
   static public  Transform _CANNONBALL_ANCHOR { get; private set; }

   [Header("Inscribed")]
   public GameObject cannonballPrefab;
   // public GameObject shotPoint;
   public Transform  shotTrans;
   public GameObject onObjects, offObjects;

   [Header( "Dynamic" )][OnValueChanged("UpdateVisuals")]
   [SerializeField] private bool      _isOn;
   [SerializeField] private Vector3   aimPoint = Vector3.negativeInfinity;
   
   public CaptainStrategy_SO captainSO { private get; set; }

   private Rigidbody shipRigidbody; // Used to apply Ship velocity to cannonballs

   public float timeToFire { get; private set; }

   public bool isOn {
      get { return _isOn; }
      set {
         _isOn = value;
         UpdateVisuals();
      }
   }

#if UNITY_EDITOR
   private void OnValidate() {
      UnityEditor.EditorApplication.delayCall += UpdateVisuals;
   }
#endif
   
   void UpdateVisuals() {  
         if (onObjects  != null) onObjects.SetActive( _isOn );
         if (offObjects != null) offObjects.SetActive( !_isOn );
   }

   private void Awake() {
      timeToFire = -1;
   }

   public void SetShipRigidbody(Rigidbody tRB) {
      shipRigidbody = tRB;
   }

   public void Fire( Vector3 tAimPoint, float tTF = 0 ) {
      timeToFire = Time.time + tTF;
      aimPoint = tAimPoint;
   }

   public void Update() {
      if (timeToFire > 0) {
         if (Time.time >= timeToFire) {
            ActuallyFire();
         }
      }
   }

   private void ActuallyFire() {
      if ( _CANNONBALL_ANCHOR == null ) {
         GameObject go = new GameObject("_Cannonball_Anchor_");
         _CANNONBALL_ANCHOR = go.transform;
      }
      
      if ( _CANNON_MIN_DOT_FORWARD == 0 ) {
         SpartanSwashbucklersSettings_SO settings = GameManager.SETTINGS;
         _CANNON_AIM_DEGREES = settings.cannonAimDegrees;
         _CANNON_MIN_DOT_FORWARD = Mathf.Cos( _CANNON_AIM_DEGREES * Mathf.Deg2Rad );
         _CANNON_MAX_DOT_RIGHT = Mathf.Sin( _CANNON_AIM_DEGREES   * Mathf.Deg2Rad );
      } 
      timeToFire = -1;
      GameObject cbGO = Instantiate<GameObject>(cannonballPrefab, shotTrans.position,
         shotTrans.rotation, _CANNONBALL_ANCHOR);
      Cannonball cb = cbGO.GetComponent<Cannonball>();
      cb.captainSO = captainSO;
      cb.shipVelWhenFired = shipRigidbody.velocity;
      if ( aimPoint == Vector3.negativeInfinity ) return;
      
      // Attempt to aim at aimPoint if it is not negativeInfinity
      // When firing at a targeted point, don't add ship velocity to Cannonball
      cb.shipVelocityApplied = 0;
      aimPoint.y = shotTrans.position.y; // Flatten the aimPoint to be the same y as the shotTrans
      Vector3 delta = aimPoint - shotTrans.position;
      delta.Normalize();
      Transform cbTrans = cbGO.transform;
      if ( Vector3.Dot( delta, cbTrans.forward ) >= _CANNON_MIN_DOT_FORWARD ) {
         cbGO.transform.LookAt(aimPoint); // LookAt the flattened aimPoint
         return;
      }
      // Can't possibly aim at the aimPoint, so get as close as possible.
      Vector3 possRelativeAimPoint = shotTrans.forward * _CANNON_MIN_DOT_FORWARD;
      if ( Vector3.Dot( delta, cbTrans.right ) > 0 ) {
         possRelativeAimPoint += shotTrans.right * _CANNON_MAX_DOT_RIGHT;
      } else {
         possRelativeAimPoint -= shotTrans.right * _CANNON_MAX_DOT_RIGHT;
      }
      cbGO.transform.LookAt(shotTrans.position + possRelativeAimPoint);
   }

   public float dotForward( Vector3 aimV ) {
      aimV.y = shotTrans.position.y;
      Vector3 delta = aimV - shotTrans.position;
      delta.Normalize();
      return Vector3.Dot( shotTrans.forward, delta );
   }

   public float dotRight( Vector3 aimV ) {
      aimV.y = shotTrans.position.y;
      Vector3 delta = aimV - shotTrans.position;
      delta.Normalize();
      return Vector3.Dot( shotTrans.right, delta );
   }
}
