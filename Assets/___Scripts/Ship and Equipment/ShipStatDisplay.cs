using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ShipStatDisplay : MonoBehaviour {
    static private float GMAX_HEALTH =1, GMAX_CREW =1, GMAX_CANNON =1;

    public Ship         ship;
    
    private                  ShipSettings          sst;
    [SerializeField] private XnStatBarRadialSprite barHealth, barCrew, barCannon;
    [SerializeField] private TMP_Text              arcText, initialsText;
    [SerializeField] private SpriteRenderer        colorLeft, colorRight, topBackground;

    private bool updateArcText = true; 
    
    private void OnValidate() {
        if ( ship == null ) GetComponentInParent<Ship>();
    }

    void Start() {
        if ( ship == null ) {
            Debug.LogError( "The ShipStatDisplay component needs a Ship assigned to it." );
            return;
        }
        // Set up some variables.
        sst = GameManager.SETTINGS.shipSettingsDict[ship.shipClass];
        if ( sst.healthMax > GMAX_HEALTH ) GMAX_HEALTH = sst.healthMax;
        if ( sst.crewMax   > GMAX_CREW ) GMAX_CREW = sst.crewMax;
        if ( sst.cannonMax > GMAX_CANNON ) GMAX_CANNON = sst.cannonMax;
        arcText.text = "";
        
        // Colors for Swashbucklers
        if ( ship.shipClass == eShipClass.swashbuckler ) {
            colorLeft.color = ship.primaryColor;
            colorRight.color = ship.secondaryColor;
            initialsText.text = ship.initials;
        } else {
            colorLeft.enabled = false;
            colorRight.enabled = false;
            topBackground.enabled = false;
            initialsText.text = $"${ship.loot:N0}";// $"<color=#000000>${ship.loot:N0}</color>";
        }
    }

    private void Update() {
        transform.rotation = Quaternion.identity;
        // Health
        barHealth.maxValue = sst.healthMax / GMAX_HEALTH;
        barHealth.value = ship.health      / sst.healthMax;
        // Crew
        barCrew.maxValue = sst.crewMax / GMAX_CREW;
        barCrew.value = ship.crew      / sst.crewMax;
        // Cannon
        barCannon.maxValue = sst.cannonMax           / GMAX_CANNON;
        barCannon.value = Mathf.Floor( ship.cannon ) / GMAX_CANNON;

        if ( updateArcText && ship != null && ship.captainSO != null) {
            if ( ship.shipClass == eShipClass.swashbuckler ) {
                arcText.text = $"<align=\"left\">#{ship.shipID} – {ship.captainSO.captainName}</align>"
                               + $"<space=4em><align=\"right\">${ship.loot}</align>";
            } else {
                arcText.text = $"#{ship.shipID} – {ship.captainSO.captainName}";
            }
            updateArcText = false;
        }
    }
}
