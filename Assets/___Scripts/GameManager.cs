using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using UnityEngine.Diagnostics;
using XnTools;
using Random = UnityEngine.Random;

//TODO: Make GameManager an internal class so that it is a friend to all classes.
// In order to do this, a namespace Swashbucklers will need to be added to everything. - JGB 2023-04-23

[DefaultExecutionOrder( -500 )] // From: https://stackoverflow.com/questions/27928474/programmatically-define-execution-order-of-scripts
public class GameManager : MonoBehaviour {
#region PUBLIC_REGION - You may use anything in here
    static public SpartanSwashbucklersSettings_SO SETTINGS { get; private set; }

    /// <summary>
    /// You can use this static method to start coroutines from your Captain_SO script.
    /// See CS_JGB_Merchant_SO for an example of this.
    /// </summary>
    /// <param name="coroutineMethodCall">A standard call to a couroutine method. You cannot pass in a string.</param>
    static public void START_COROUTINE( IEnumerator coroutineMethodCall ) {
        if ( _S == null ) {
            Debug.LogError("Cannot start coroutine from GameManager because singleton does not exist!");
            return;
        }
        _S.StartCoroutine( coroutineMethodCall );
    }
#endregion
    
#region PRIVATE_REGION - You may not use anything in here
    static private GameManager _S { get; set; }

    public bool spawnSwashbucklerShips = false;

    [SerializeField][Expandable]
    private SpartanSwashbucklersSettings_SO settingsSo;
    private Dictionary<CaptainStrategy_SO, CaptainRec> captainRecsDictRef;
    private List<CaptainStrategy_SO>                   zombiePhoenix = new List<CaptainStrategy_SO>();
    

    void Awake() {
        // Set the Singleton
        if ( _S != null ) {
            Debug.LogError( $"Cannot have two GameManager scripts in the scene. One is on {_S.gameObject.name}, and another is on {gameObject.name}." );
            return;
        }
        _S = this;
        SETTINGS = settingsSo;
        captainRecsDictRef = SETTINGS.InitSwashbucklerCaptains();
        if (spawnSwashbucklerShips) InitSwashbucklerCaptains();

        RandomizePorts();
        PublicInfo.SET_PUBLIC_GAME_MANAGER_INFO();

        Time.timeScale = SETTINGS.GetTimeScale();
    }

    void InitSwashbucklerCaptains() {
        zombiePhoenix = new List<CaptainStrategy_SO>();
        foreach ( CaptainStrategy_SO csso in captainRecsDictRef.Keys ) {
            zombiePhoenix.Add( csso );
        }
    }

    static public void SHIP_DIED( Ship ship, CaptainStrategy_SO capn ) {
        if ( _S == null ) {
            // Debug.LogError( "SHIP_DIED() was called, but GameManager._S was null!" );
            return;
        } 
            
        bool playerShipDied = SETTINGS.captainRecDict.ContainsKey( capn );
            
        // Only Pirate Captains can be reborn!
        if (playerShipDied) _S.zombiePhoenix.Add( capn );
        
        SeaCamTargetGrouper.UpdateCaptainsFocused();
    }

    private void FixedUpdate() {
        _CURR_SLOT = ( _CURR_SLOT + 1 ) % NUM_AI_SLOTS;
        
        // Spawn any Swashbuckers that need to be spawned
        if ( zombiePhoenix.Count > 0 ) {
            int ndx = Random.Range( 0, zombiePhoenix.Count );
            CaptainStrategy_SO csso = zombiePhoenix[ndx];
            // Only spawn captains that should be spawned. If spawnInGame == false, just leave in the queue
            if ( captainRecsDictRef.ContainsKey( csso ) && captainRecsDictRef[csso].spawnInGame ) {
                SpawnShip( zombiePhoenix[ndx] );
                zombiePhoenix.RemoveAt( ndx );
            }
        }
    }

    static private Transform __SWASHBUCKLERS__Transform = null;
    
    void SpawnShip( CaptainStrategy_SO csso ) {
        // Check parent Transform
        if ( __SWASHBUCKLERS__Transform == null ) {
            GameObject go = new GameObject( "__SWASHBUCKLERS__" );
            __SWASHBUCKLERS__Transform = go.transform;
        }
        
        GameObject shipPrefab = GameManager.SETTINGS.shipSettingsDict[eShipClass.swashbuckler].prefab;
        GameObject shipGO = Instantiate<GameObject>( shipPrefab, __SWASHBUCKLERS__Transform );
        Ship ship = shipGO.GetComponent<Ship>();
        ship.thisIsANonSpawnedTestShip = false;
        if ( captainRecsDictRef.TryGetValue( csso, out CaptainRec capnRec ) ) {
            ship.primaryColor = capnRec.primaryColor;
            ship.secondaryColor = capnRec.secondaryColor;
            ship.initials = capnRec.initials;
            shipGO.name = $"S{ship.shipID}-{csso.captainName}";
        } else {
            Debug.LogError($"SpawnShip called on CaptainStrategy_SO {csso.name}, but it did not have an entry in captainRecsDictRef.");
            return;
        }
        
        // Pick a random location on the edge of the board
        Vector2Int mapLoc = Vector2Int.zero;
        int max = SeaMap.TEXTURE_RESOLUTION - 1;
        if ( Random.value > 0.5f ) {
            if ( Random.value > 0.5f ) {    // Left side
                mapLoc.x = 0;
            } else {                        // Right side
                mapLoc.x = max;
            }
            do {
                mapLoc.y = Random.Range( 1, max );
            } while ( !SeaMap.NAVIGABLE[mapLoc.x, mapLoc.y] );
        } else {
            if ( Random.value > 0.5f ) {    // Bottom
                mapLoc.y = 0;
            } else {                        // Top
                mapLoc.y = max;
            }
            do {
                mapLoc.x = Random.Range( 1, max );
            } while ( !SeaMap.NAVIGABLE[mapLoc.x, mapLoc.y] );
        }
        
        Vector3 randomOffset = Random.insideUnitSphere * (SeaMap.TERRAIN_SCALE * 0.5f);
        randomOffset.y = 0;
        shipGO.transform.position = SeaMap.Position( mapLoc ) + randomOffset;
        shipGO.transform.rotation = Quaternion.Euler( 0, Random.Range( 0, 360 ), 0 );
        ship.captainSO = csso;
        captainRecsDictRef[csso].currentShip = ship;
        SeaCamTargetGrouper.UpdateCaptainsFocused();
    }

    private void OnDestroy() {
        // Nullify the Singleton
        if ( _S == this ) {
            _S = null;
            SETTINGS = null;
        }
    }


    private List<PortInfo> portsList;
    public 
    void RandomizePorts() {
        Color portColor = new Color( 1, 0, 0, 0.5f ); 
        Texture2D tex = SETTINGS.seaMapTexture2D;
        Color[] pixels = tex.GetPixels();
        

        // Find all of the ports embedded into the image
        int ndx = 0;
        PortInfo pI;
        portsList = new List<PortInfo>();
        for (int v = 0; v < tex.height; v++) {
            for (int h = 0; h < tex.width; h++, ndx++) {
                if ( pixels[ndx] == portColor ) {
                    pI = new PortInfo();
                    pI.gridLoc = new Vector2Int( h, v );
                    portsList.Add( pI );
                }
            }
        }
        
    }

    private class PortInfo {
        public Vector2Int gridLoc;
        public eMapSense  portType = eMapSense.port;
    }

    static public void DistributeLoot( Ship ship, Dictionary<CaptainStrategy_SO, float> damageDict ) {
        if ( !ship.CheckDamageDict( damageDict ) ) {
            Debug.LogWarning($"SHENANIGANS! Something is trying to DistributeLoot(), but the damageDict passed in isn't" +
                             $"the same as the damageDict on the ship.");
            return;
        }
        float totalDamage = 0;
        foreach ( float dmg in damageDict.Values ) {
            totalDamage += dmg;
        }

        bool shipSunkWasPlayer = (ship.shipClass == eShipClass.swashbuckler)
                                 && SETTINGS.captainRecDict.ContainsKey( ship.captainSO );
        bool killerWasPlayer = false;
        
        float totalLoot = ship.loot;

        CaptainRec capnRec;
        foreach ( KeyValuePair<CaptainStrategy_SO, float> kvp in damageDict ) {
            if ( kvp.Key == null ) {
                Debug.LogError( $"Ship: {ship.name} passed in a damageDict with a NULL CaptainStrategy_SO\n"
                                + JsonUtility.ToJson( damageDict ) );
                continue;
            }
            if ( !SETTINGS.captainRecDict.ContainsKey( kvp.Key ) ) {
                continue; // It's not a player captain that did damage, so ignore it
            }
            capnRec = SETTINGS.captainRecDict[kvp.Key]; 
            // We know that this kvp.Key captainSO that did damage was a player
            killerWasPlayer = true;
            if ( capnRec == null ) {
                Debug.LogError( $"Ship: {ship.name} passed in a damageDict with a NULL CaptainRec for CSSO: {kvp.Key.name}\n"
                                + JsonUtility.ToJson( damageDict ) );
                continue;
            }
            if ( capnRec.currentShip != null ) {
                capnRec.currentShip.loot += totalLoot * (kvp.Value / totalDamage);
            }
            if ( shipSunkWasPlayer ) {
                SETTINGS.captainRecDict[kvp.Key].killsPVP += 1;
            } else {
                SETTINGS.captainRecDict[kvp.Key].killsMerchant += 1;
            }
        }

        if ( shipSunkWasPlayer ) {
            if ( killerWasPlayer ) {
                SETTINGS.captainRecDict[ship.captainSO].deathsPVP += 1;
            } else {
                SETTINGS.captainRecDict[ship.captainSO].deathsNonPVP += 1;
            }
        }
    }

    #region Banking Money
    
    static private XnSparseGrid<bool> havenGrid;
    static public void SetHavenGrid( List<Vector2Int> portLocs ) {
        havenGrid = new XnSparseGrid<bool>();
        foreach ( Vector2Int v2 in portLocs ) {
            havenGrid[v2.x, v2.y] = true;
        }
    }
    static public void AttemptBankMoney( Ship ship, Vector2Int mapLoc ) {
        // If mapLoc isn't a Pirate Haven, return immediately
        if ( !havenGrid[mapLoc] ) return;

        if ( ship.loot > 0 && SETTINGS.captainRecDict.ContainsKey(ship.captainSO) ) {
            CaptainRec cRec = SETTINGS.captainRecDict[ship.captainSO];
            cRec.moneyBanked += Mathf.RoundToInt(ship.loot);
            ship.loot = 0;
        }
    }
    
    #endregion
    
    
    
    #region AI_SLOTS
    
    static private List<List<Ship>> _AI_SLOTS;

    private const  int  NUM_AI_SLOTS     = 10;
    static private int  _CURR_SLOT       = 0;
    static private bool _AI_SLOTS_INITED = false;

    private static void InitAISlots() {
        if ( _AI_SLOTS_INITED ) return;
        _AI_SLOTS = new List<List<Ship>>(NUM_AI_SLOTS);
        for ( int i = 0; i < NUM_AI_SLOTS; i++ ) {
            _AI_SLOTS.Add( new List<Ship>() );
        }
        _AI_SLOTS_INITED = true;
    } 
    
    public static int RegisterForAISlot( Ship s ) {
        if (!_AI_SLOTS_INITED) InitAISlots();
        // Iterate over all the slots to see which has an opening
        int minCount = System.Int32.MaxValue;
        int minIndex = -1;
        for ( int i = 0; i < NUM_AI_SLOTS; i++ ) {
            if ( _AI_SLOTS[i].Count < minCount ) {
                minCount = _AI_SLOTS[i].Count;
                minIndex = i;
            }
        }
        if ( minIndex != -1 ) {
            _AI_SLOTS[minIndex].Add( s );
        }
        return minIndex;
    }

    public static bool IsItMyTurn( Ship s ) {
        return ( _AI_SLOTS[_CURR_SLOT].IndexOf( s ) != -1 );
    }
        
    #endregion
#endregion
}


