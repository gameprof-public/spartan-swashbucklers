using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XnTools;


static public class ShipDoctor {
    static private XnSparseGrid<bool>      havenGrid;
    static private Dictionary<Ship, float> lastTimeHealed;

    static public void Init( List<Vector2Int> portLocs ) {
        havenGrid = new XnSparseGrid<bool>();
        foreach ( Vector2Int v2 in portLocs ) {
            havenGrid[v2.x, v2.y] = true;
        }

        lastTimeHealed = new Dictionary<Ship, float>();
    }

    static public void AttemptHeal( Ship ship, Vector2Int mapLoc ) {
        // If mapLoc isn't a Pirate Haven, return with no heal
        if ( !havenGrid[mapLoc] ) return;
        
        if ( !lastTimeHealed.ContainsKey( ship ) ) {
            lastTimeHealed[ship] = -1000;
        }
        
        // If this ship has not healed too recently, heal the ship!
        if ( Time.time - lastTimeHealed[ship] > GameManager.SETTINGS.delayBetweenHavenHeals ) {
            lastTimeHealed[ship] = Time.time;
            ship.health = ship.sst.healthMax;
            ship.crew = ship.sst.crewMax;
        }
    }
}
