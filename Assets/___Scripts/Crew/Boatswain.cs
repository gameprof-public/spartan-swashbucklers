using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class Boatswain : MonoBehaviour {
    public InfoProperty info = new InfoProperty("Info",
        "The boatswain (pronounced <i>BO-sun</i>) was in charge of sails aboard ship. " +
        "In this game, the boatswain manages deploying and stowing the sail as well as" +
        " repairing sails when needed (repair speed depends on # of crew.). "           +
        "When sailsUp == true, the sails are deployed.", true, false);
    [OnValueChanged("SailsUpChangedCallback")]
    [SerializeField] private bool _sailsUp = true;
    public GameObject[] sailsUpObjects;
    public GameObject[] sailsDownObjects;


    void Start() {
        sailsUp = _sailsUp;
    }

    public bool sailsUp {
        get { return _sailsUp; }
        set {
            _sailsUp = value;
            foreach (GameObject go in sailsUpObjects) { go.SetActive(_sailsUp); }
            foreach (GameObject go in sailsDownObjects) { go.SetActive(!_sailsUp); }
        }
    }

    void SailsUpChangedCallback() {
        sailsUp = _sailsUp;
    }
}
