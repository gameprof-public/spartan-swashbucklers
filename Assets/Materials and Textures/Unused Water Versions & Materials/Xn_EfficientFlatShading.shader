// Shader written with ChatGPT. - JGB 2023-04-18
Shader "Xn/EfficientFlatShading"
{
    Properties
    {
        _MainColor ("Color", Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200
        
        Pass {
            Cull Back
            ZWrite On
            ZTest LEqual

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            struct appdata {
                float4 vertex : POSITION;
            };

            struct v2f {
                float4 vertex : SV_POSITION;
                fixed4 color : COLOR;
            };

            fixed4 _MainColor;
            
            v2f vert (appdata v) {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.color = _MainColor;
                return o;
            }


            fixed4 frag (v2f i) : SV_Target {
                return _MainColor;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
}
