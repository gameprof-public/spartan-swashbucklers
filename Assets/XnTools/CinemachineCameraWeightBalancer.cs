using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using XnTools;
using Debug = UnityEngine.Debug;

namespace Cinemachine {
    internal class CinemachineCameraWeightBalancer : MonoBehaviour {
        public CinemachineMixingCamera cineMixCam;

        public enum eMixMode { free, sumTo100, linearInterp };

        public eMixMode mixMode = eMixMode.free;

        [NaughtyAttributes.ReadOnly]
        public float weightSum = 1;

        public List<Weight> weights;
        

        [HideIf("cineMixCamIsSet")]
        [Button( "GetComponent<CinemachineMixingCamera>()" )]
        void GetCinemachineMixingCamera() {
            CinemachineMixingCamera cmc = GetComponent<CinemachineMixingCamera>();
            if ( cmc == null ) {
                Debug.LogError( $"There is no CinemachineMixingCamera component on the GameObject {gameObject.name}." );
                return;
            }
            cineMixCam = cmc;
        }

        private bool cineMixCamIsSet => cineMixCam != null;
        
        [ShowIf("cineMixCamIsSet")]
        [Button( "Link to Child Cameras" )]
        void LinkToChildCameras() {
            CinemachineVirtualCameraBase[] vCams = cineMixCam.ChildCameras;
            List<Weight> oldWeights = weights.GetRange(..); // This uses the GetRange overload in XnExtensionMethods
            weights.Clear();
            Weight w;
            for ( int i = 0; i < vCams.Length; i++ ) {
                w = new Weight();
                w.vCam = vCams[i];
                w.name = w.vCam.name;
                w.weight = w.oldWeight = cineMixCam.GetWeight( w.vCam );
                foreach ( Weight wOld in oldWeights ) {
                    if ( wOld.vCam == w.vCam ) {
                        w.weight = wOld.weight;
                    }
                }
                weights.Add( w );
            }

            ProcessWeights();
        }

        
        private void OnValidate() {
            ProcessWeights();
        }

        void Start() {
            if ( mixMode == eMixMode.linearInterp ) {
                interpolation = 100 * currentStep / numInterpolationSteps;
                desiredStep = (int) currentStep;
                SetActiveByInterpolation( true );
                ProcessWeights();
                
            }
            
        }

        void Update() {
            
            if ( mixMode == eMixMode.linearInterp ) {
                KeyboardInput();
            }
        }

        void KeyboardInput() {
            Vector2 mouseScrollDelta = Input.mouseScrollDelta;
#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
            mouseScrollDelta.y *= -1;
#endif
            
            if ( Input.GetKeyDown( zoomInKey ) || mouseScrollDelta.y > 0 ) {
                desiredStep -= 1;
            }
            if ( Input.GetKeyDown( zoomOutKey ) || mouseScrollDelta.y < 0 ) {
                desiredStep += 1;
            }
            desiredStep = Mathf.Clamp( desiredStep, 0, numInterpolationSteps );
        }
        
        
        void FixedUpdate() {
            if ( mixMode == eMixMode.linearInterp ) {
                InterpolateCamera();
            }
        }

        void InterpolateCamera() {
            if ( currentStep.IsApproximately( desiredStep, 0.001f ) ) {
                // We're already here and don't need to adjust camera
                return;
            }
            if ( currentStep.IsApproximately( desiredStep, 0.01f ) ) {
                currentStep = desiredStep;
                interpolation = 100 * currentStep / numInterpolationSteps;
                ProcessWeights();
                return;
            }
            // We need to interpolate
            currentStep = (1 - interpU) * currentStep + interpU * desiredStep;
            interpolation = 100 * currentStep / numInterpolationSteps;
            ProcessWeights();
        }
        
        

        internal void ProcessWeights() {
            if ( weights == null || weights.Count == 0 ) return;
            
            // Find the changing weight
            Weight wChanged = null;
            
            weightSum = 0;
            foreach ( Weight w in weights ) {
                weightSum += w.weight;
                if ( !w.oldWeight.IsApproximately(w.weight, 0.005f) ) {
                    wChanged = w;
                }
            }
            switch ( mixMode ) {
            case eMixMode.sumTo100:
                // Avoid the issue with the weight coming back down from 100
                if ( wChanged != null && wChanged.oldWeight.IsApproximately(100, 0.001f) && wChanged.weight < 100 ) {
                    foreach ( Weight w in weights ) {
                        if ( w == wChanged ) continue;
                        w.weight = 1;
                        weightSum += 1;
                    }
                }
                float weightToAdjust = weightSum;
                float adjustedWeight = 100;
                if ( wChanged != null ) {
                    weightToAdjust -= wChanged.weight;
                    adjustedWeight -= wChanged.weight;
                }
                float weightMultiplier = (weightToAdjust == 0) ? 0 : adjustedWeight / weightToAdjust;
                foreach ( Weight w in weights ) {
                    if ( w == wChanged ) continue;
                    w.weight *= weightMultiplier;
                }
                weightSum = 100;
                break;
            
            case eMixMode.linearInterp:
                // Any changes to weights are ignored in linearInterp mode
                if ( weights.Count == 1 ) {
                    weights[0].weight = 100;
                    cineMixCam.SetWeight(weights[0].vCam, 100);
                    break;
                }
                
                float u = interpolation * 0.01f;
                u *= weights.Count-1;
                int ndx0 = Mathf.FloorToInt( u );
                float u100 = (u - ndx0) * 100;
                for (int i=0; i<weights.Count; i++) {
                    if ( i < ndx0 ) {
                        weights[i].weight = 0;
                    } else if ( i == ndx0 ) {
                        weights[i].weight = 100 - u100;
                    } else if ( i == ndx0 + 1 ) {
                        weights[i].weight = u100;
                    } else {
                        weights[i].weight = 0;
                    }
                }
                break;
            
            case eMixMode.free:
            default:
                // Do nothing
                break;
            
            }
            
            // Apply these weights back to the cineMixCam
            try {
                foreach ( Weight w in weights ) {
                    cineMixCam.SetWeight( w.vCam, w.weight );
                    w.oldWeight = w.weight;
                }
                // if ( !Application.isPlaying && Application.isEditor ) {
                //     cineBrain?.ManualUpdate();
                // }
            }
            catch { }
            SetActiveByInterpolation();
        }
        
        
#region Activate GameObjects based on Interpolation
        [ShowIf( "linearInterpolation" )][BoxGroup("Linear Interpolation Mix Mode")]
        [Range( 0, 100 )]
        public float interpolation = 0;
        private bool linearInterpolation => mixMode == eMixMode.linearInterp;
        [Header("Keyboard Control")]
        [ShowIf( "linearInterpolation" )][BoxGroup("Linear Interpolation Mix Mode")]
        public int numInterpolationSteps = 20;
        [ShowIf( "linearInterpolation" )] [BoxGroup( "Linear Interpolation Mix Mode" )] [Range(0,1)]
        [Tooltip("This determines how quickly the camera moves to the next step when keyboard controls are used. 0=never, 1=immediately")]
        public float interpU = 0.2f; 
        [ShowIf( "linearInterpolation" )][BoxGroup("Linear Interpolation Mix Mode")]
        public float currentStep = 5;
        private int desiredStep = 5;
        [ShowIf( "linearInterpolation" )][BoxGroup("Linear Interpolation Mix Mode")]
        public KeyCode zoomInKey = KeyCode.Comma, zoomOutKey = KeyCode.Period;
        [Header("Set Visibility of GameObjects by Interpolation Value")]
        [ShowIf("linearInterpolation")][BoxGroup("Linear Interpolation Mix Mode")]
        [MinMaxSlider( 0, 100 )][Label("Visible Range    (0-100)")]
        public Vector2 visibleRange;
        [ShowIf("linearInterpolation")][BoxGroup("Linear Interpolation Mix Mode")]
        public  GameObject[] gameObjects;
        private bool         currentlyActive = false;

        void SetActiveByInterpolation( bool forceUpdate = false ) {
            bool shouldBeActive = ( interpolation >= visibleRange[0] && interpolation <= visibleRange[1] );
            if ( forceUpdate || (shouldBeActive != currentlyActive) ) {
                currentlyActive = shouldBeActive;
                foreach ( GameObject go in gameObjects ) {
                    go.SetActive(shouldBeActive);
                }
            }
        }
        
#endregion
    }
    
    

    [System.Serializable]
    public class Weight {
        [HideInInspector]
        public string name;
        [Range(0,100)]
        public  float  weight;
        [HideInInspector]
        public float oldWeight;
        [HideInInspector]
        public CinemachineVirtualCameraBase vCam;
    }
}