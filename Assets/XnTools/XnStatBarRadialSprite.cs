using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XnStatBarRadialSprite : MonoBehaviour {
    public Transform fillSpriteTrans, maxSpriteTrans;
    public Vector2   minMaxRotation = new Vector2( -180, 0 );
    [Range( 0, 1 )]
    public float value = 0.5f, maxValue = 1;

    private SpriteRenderer fillSRend,         maxSRend;
    private float          _lastVal     = -1, _lastMaxVal = -1;
    private bool           updateColors = false;

    void UpdateRotation() {
        if ( fillSpriteTrans == null || maxSpriteTrans == null ) {
            fillSpriteTrans = transform.Find( "Bar" );
            maxSpriteTrans = transform.Find( "Bar Max" );
            fillSRend = fillSpriteTrans.GetComponent<SpriteRenderer>();
            maxSRend = maxSpriteTrans.GetComponent<SpriteRenderer>();
        }
        if ( fillSpriteTrans == null ) return;
        
        float rZ;
        if ( ( maxValue != _lastMaxVal ) && ( maxSpriteTrans != null )) {
            if ( updateColors ) {
                if ( ( maxSRend != null ) && ( fillSRend != null ) ) maxSRend.color = Color.Lerp( fillSRend.color, Color.black, 0.5f );
                updateColors = false;
            }
            rZ = ( 1 - maxValue ) * minMaxRotation.x + maxValue * minMaxRotation.y;
            maxSpriteTrans.localRotation = Quaternion.Euler(0, 0, rZ);
            _lastMaxVal = maxValue;
        }
        if ( value != _lastVal ) {
            float scaledVal = value * maxValue; // TODO: Change this to work better with actual numbers.
            rZ = ( 1 - scaledVal ) * minMaxRotation.x + scaledVal * minMaxRotation.y;
            fillSpriteTrans.localRotation = Quaternion.Euler(0, 0, rZ);
            _lastVal = value;
        }
    }
    
    // Update is called once per frame
    void Update() {
        UpdateRotation();
    }

    private void OnValidate() {
        updateColors = true;
        UpdateRotation();
    }
}
