using NaughtyAttributes;
using UnityEngine;
using System.Collections.Generic;
using UnityEditor;

namespace XnTools.XnAI {
    public enum eStatus { failed, busy, success };
    
    [System.Serializable]
    public abstract class iAINode {
        public const int    noNode                  = -1;
        public const int    allNodes                = -2;
        public const string nodeNameNotSet          = "***Node Name Not Set!***";
        public const string nodeSelectText          = "Select Node";
        public const string nodeDeselectText        = "Deselect Node";
        public const char   nullMethodChar          = ' ';
        public const string nullMethodName          = " null";
        public const int    treeStatusIndentSpaces  = 4;
        public const string treeStatusIndentBullets = "  >*-~>*-~>*-~>*-~";


        static public aFloat defaultUtility = 0.5f;

        public string _nodeName = iAINode.nodeNameNotSet;
        public string nodeName {
            get { return _nodeName; }
            set { if (_nodeName == iAINode.nodeNameNotSet) _nodeName = value; }
        }
        
        
        public abstract eStatus status   { get; protected set; }
        protected string statusLetter {
            get {
                if ( status == eStatus.success ) return"<color=#006600><b>Success</b></color>";
                if ( status == eStatus.failed ) return"<color=#990000><b>Failed</b></color>";
                if ( status == eStatus.busy ) return"<color=#666600><b>Busy..</b></color>";
                return"_";
            }
        }
        public string statusNote = "";
        
        public abstract aFloat  utility  { get; protected set; }
        
        
        [HideInInspector][SerializeField]
        protected AITree aiTree;
        public virtual AITree GetAITree() => aiTree;
        
        [HideInInspector][SerializeField]
        protected iAINode parent;
        public virtual iAINode GetParent() => parent;


        // protected abstract void SelectMe();
        // public abstract void UpdateThisNodeSelected();
        // protected abstract string GetSelectNodeText();
        // protected abstract bool IsThisNodeSelected();
        
        protected bool isPlaying => Application.isPlaying;
#if UNITY_EDITOR
        protected bool isNotPaused => Application.isPlaying && !EditorApplication.isPaused;
#else
        protected bool isNotPaused => Application.isPlaying;
#endif
        
        [AllowNesting] [OnValueChanged( "SelectMe" )][HideIf( "isPlaying" )][Label("GetSelectNodeText")]
        public bool thisNodeSelected = false;
        protected void SelectMe() {
            if (!IsThisNodeSelected()) {
                thisNodeSelected = GetAITree().SelectNode( this );
            } else {
                thisNodeSelected = false;
                GetAITree().DeselectNode( this );
            }
        }

        protected string GetSelectNodeText() {
            return ( IsThisNodeSelected() ) ? iAINode.nodeDeselectText : iAINode.nodeSelectText;
        }

        public void UpdateThisNodeSelected() {
            thisNodeSelected = IsThisNodeSelected();
        }

        protected bool IsThisNodeSelected() {
            if ( aiTree == null ) return false;
            iAINode selNode = aiTree.GetSelectedNode();
            if ( selNode == null ) return false;
            if ( selNode != this ) return false;
            return true;
        }

        public virtual void SetAITreeAndParent( AITree tTree, iAINode tParent ) {
            thisNodeSelected = IsThisNodeSelected();
            aiTree = tTree;
            parent = tParent;
            GenerateNodeName();
        }

        // Runtime methods
        public abstract aFloat CheckUtility();
        protected bool utilityChecked = false;
        public abstract eStatus Execute(ref List<string> treeStatList, int callDepth);

        protected string GenerateStatus(int callDepth) {
            string str = new System.String( ' ', callDepth * treeStatusIndentSpaces ) // Indent
                         + treeStatusIndentBullets[callDepth] + " "
                         + _nodeName + " - " // Node Name
                         + statusLetter + " " // Status
                         + ( utilityChecked ? $"{utility:0.00} " : " " ) // Utility
                         + $"<i>{statusNote}</i>";
            return str; 
        }

        private string nonExecuteStatus;
        public string GetNodeStatusWithoutExecuting( int callDepth ) {
            // if ( nonExecuteStatus == "" ) {
                nonExecuteStatus = new System.String( ' ', callDepth * treeStatusIndentSpaces ) // Indent
                                   + treeStatusIndentBullets[callDepth] + " "
                                   + _nodeName + " - <i>n/c</i>"; // Node Name
            // }
            return nonExecuteStatus;
        }

        // public bool AddDecoratorParent() {
        //     iAINode parentNode = this.GetParent();
        //     if ( parentNode == null ) {
        //         Debug.LogError( "You can't add a Decorator above the top-level Selector." );
        //         return false;
        //     }
        //     AINode_Decorator deco = new AINode_Decorator();
        //     parentNode.ReplaceChildNode( this, deco );
        //     deco.AddChildNode( this );
        //     return true;
        // }

        public abstract bool ReplaceChildNode( iAINode oldNode, iAINode newNode );
        public abstract bool AddChildNode( iAINode node );


        public override string ToString() {
            return _nodeName;
            // return this.GetType().ToStringShort();
        }

        public void GenerateNodeName() {
            if ( _nodeName == iAINode.nodeNameNotSet ) {
                _nodeName = $"{this.GetType().ToStringShort()}-{aiTree.nextNodeNum}";
            } 
        }
        
        public virtual void AddLeaf() {
            AddChildNode( new AINode_Leaf() );
            SetAITreeAndParent( aiTree, parent );
        }

        public virtual void AddComposite() {
            AddChildNode( new AINode_Composite() );
            SetAITreeAndParent( aiTree, parent );
        }

        public virtual void AddDecorator() {
            AddChildNode( new AINode_Decorator() );
            SetAITreeAndParent( aiTree, parent );
        }

        public abstract void DeleteSelf();
        // public void DeleteSelf() {
        //     bool success = false;
        //     if ( parent != null ) {
        //         success = parent.DeleteChildNode( this );
        //     }
        //     if ( success )
        //         Debug.Log( $"The node {nodeName} was successfully removed." );
        //     else
        //         Debug.LogError( $"The node {nodeName} FAILED to be removed." );
        // }
        
        public abstract bool DeleteChildNode( iAINode node = null, iAINode replacement = null );
        


        //public abstract string nodeName { get; set; }
        //public abstract eStatus status { get; }
        //public abstract aFloat utility { get; }

        //public abstract void SetAITreeAndParent( AITree tTree, iAINode tParent );
        //public abstract bool CheckValidity();
        //public abstract float CheckUtility();
        //public abstract eStatus Execute();
        //public abstract void AddLeaf();
        //public abstract void AddComposite();
        //public abstract void AddDecorator();
        //public abstract void DeleteSelf();
        //public abstract bool DeleteChildNode( iAINode node );
        //public abstract AITree GetAITree();
        //public abstract iAINode GetParent();
        //public abstract void UpdateThisNodeSelected();

        //aFloat CheckUtility() {
        //    throw new System.NotImplementedException();
        //}

        //public abstract bool DeleteChildNode( iAINode node, iAINode replacement = null );
    }


    static public class XnAI_Extensions {
        static public eStatus Invert( this eStatus stat ) {
            switch ( stat ) {
            case eStatus.busy:
                return eStatus.busy;
            case eStatus.failed:
                return eStatus.success;
            case eStatus.success:
                return eStatus.failed;
            }
            return eStatus.failed; // This should never happen because all cases should be handled in the switch
        }

    }


    [System.Serializable]
    public struct aFloat {

        public override int GetHashCode() {
            return _value.GetHashCode();
        }

        static private bool  toStringTruncate = true;

        public float _value { get; set; }

        private aFloat( float f ) {
            _value = f;
        }

        public override string ToString() {
            return ( toStringTruncate ) ? $"{_value:0.##}" 
                : _value.ToString( System.Globalization.CultureInfo.InvariantCulture );
        } 
        
        public bool Equals( aFloat other ) {
            return _value.Equals( other._value );
        }

        public override bool Equals( object obj ) {
            return obj is aFloat other && Equals( other );
        }

        static public implicit operator float( aFloat f ) {
            return f._value;
        }

        static public implicit operator aFloat( float value ) {
            return new aFloat( value );
        }
        
        static public bool operator > (aFloat a, aFloat b) {
            return a._value > b._value;
        }
        
        static public bool operator < (aFloat a, aFloat b) {
            return a._value < b._value;
        }
        
        static public bool operator == (aFloat a, aFloat b) {
            return PreciseEquality(a._value, b._value);
        }
        static public bool operator != (aFloat a, aFloat b) {
            return !PreciseEquality(a._value, b._value);
        }
        
        static public bool operator > (aFloat a, float b) {
            return a._value > b;
        }
        
        static public bool operator < (aFloat a, float b) {
            return a._value < b;
        }
        
        static public bool operator == (aFloat a, float b) {
            return PreciseEquality(a._value, b);
        }
        static public bool operator != (aFloat a, float b) {
            return !PreciseEquality(a._value, b);
        }

        /// <summary>
        /// Compare equality while considering the limits of floating point error.
        /// From: https://roundwide.com/equality-comparison-of-floating-point-numbers-in-csharp/
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="tolerance">Default value is 1e-6</param>
        /// <returns></returns>
        static public bool PreciseEquality( float a, float b, float tolerance=(1e-6f) ) {
            float diff = Mathf.Abs(a - b);
            return diff <= tolerance ||
                   diff <= Mathf.Max(Mathf.Abs(a), Mathf.Abs(b)) * tolerance;
        }
    }
    
    /*
    public interface iAINode {
        public const int    noNode           = -1;
        public const int    allNodes         = -2;
        public const string nodeNameNotSet   = "***Node Name Not Set!***";
        public const string nodeSelectText   = "Select Node";
        public const string nodeDeselectText = "Deselect Node";
        public const char   nullMethodChar   = ' ';
        public const string nullMethodName   = " null";
        
        static public aFloat defaultUtility   = 0.5f;

        public string  nodeName { get; set; }
        public eStatus status   { get; }
        public aFloat  utility  { get; }
        public void    SetAITreeAndParent( AITree tTree, iAINode tParent );
        public AITree  GetAITree();
        public iAINode GetParent();
        public void    UpdateThisNodeSelected();
        // public eValidity GetValid();
        
        // Runtime methods
        public aFloat CheckUtility();
        public eStatus Execute();
        
        public void AddLeaf();
        public void AddComposite();
        public void AddDecorator();
        public void DeleteSelf();
        public bool DeleteChildNode( iAINode node, iAINode replacement=null );

        public bool AddDecoratorParent() {
            iAINode parentNode = this.GetParent();
            if ( parentNode == null ) {
                Debug.LogError("You can't add a Decorator above the top-level Selector."  );
                return false;
            }
            AINode_Decorator deco = new AINode_Decorator();
            parentNode.ReplaceChildNode( this, deco );
            ((iAINode) deco).AddChildNode( this );
            return true;
        }

        public bool ReplaceChildNode( iAINode oldNode, iAINode newNode ) {
            if ( this is AINode_Decorator ) {
                AINode_Decorator decorator = (AINode_Decorator) this;
                if ( decorator.childNode == oldNode ) {
                    decorator.childNode = newNode;
                    return true;
                }
                return false;
            }
            if ( this is AINode_Composite ) {
                AINode_Composite composite = (AINode_Composite) this;
                int ndx = composite.childList.IndexOf( oldNode );
                if ( ndx != -1 ) {
                    composite.childList[ndx] = newNode;
                    return true;
                }
                return false;
            }
            // AINode_Leafs don't have children, so an AINode_Leaf version is not possible
            return false;
        }

        public bool AddChildNode( iAINode node ) {
            if ( this is AINode_Decorator ) {
                ( this as AINode_Decorator ).childNode = node;
                return true;
            } else if ( this is AINode_Composite ) {
                ( this as AINode_Composite ).childList.Add( node );
                return true;
            }
            // } else if ( this is AINode_Leaf ) { // Leafs don't have children, so not possible
            return false;
        }

        public string ToString() {
            return this.GetType().ToStringShort();
        }

        public string GenerateNodeName() {
            return $"{this.GetType().ToStringShort()}-{GetAITree().nextNodeNum}";
        }
        
    }
    */

}

// [System.Serializable]
// public class AINode { //: iAINode {
//
//     public string _nodeName = iAINode.nodeNameNotSet;
//     public string nodeName {
//         get { return _nodeName; }
//         set { _nodeName = value; }
//     }
//     
//     protected bool isPlaying  => true; //Application.isPlaying;
//     protected bool isSelector => this.GetType() == typeof(AINode_Composite);
//     protected bool isDecorator => this.GetType() == typeof(AINode_Decorator);
//     protected bool isLeaf => this.GetType() == typeof(AINode_Leaf);
//
//     // [AllowNesting] [OnValueChanged( "SelectMe" )]
//     // public bool thisNodeSelected = false;
//     // void SelectMe() {
//     //     if (thisNodeSelected) aiTree.SelectNode( this );
//     //     thisNodeSelected = false;
//     // }
//     
//     [ShowIf("isPlaying")][AllowNesting]
//     public    bool   valid    = false;
//     [ShowIf("isPlaying")][AllowNesting]
//     public    float  utility  = 0;
//     [NonSerialized] protected iAINode parent;
//     [NonSerialized] protected AITree  aiTree;
//     
//
//
//     [AllowNesting][ShowIf("isSelector")]
//     public eCompositeType compositeType = eCompositeType.sequence;
//     [AllowNesting][ShowIf("isSelector")]
//     public List<iAINode> childList   = new List<iAINode>();
//     [AllowNesting][ShowIf(EConditionOperator.And, "isSelector", "isPlaying")]
//     public int          currentNode = iAINode.noNode;
//     
//     
//     [AllowNesting][ShowIf("isDecorator")]
//     public AINode child;
//
//     public virtual void SetAITreeAndParent( AITree tTree, iAINode tParent ) {
//         aiTree = tTree;
//         parent = tParent;
//         _nodeName = this.GetType().ToString();
//     }
//
//     public virtual bool CheckValidity() {
//         return false;
//     }
//
//     public virtual eStatus Execute() {
//         return eStatus.failed;
//     }
//
//     public virtual float CheckUtility() {
//         return 0;
//     }
//
//     public virtual void AddLeaf() { }
//     public virtual void AddComposite() { }
//     public virtual void AddDecorator() { }
//
//     public virtual void DeleteSelf() {
//         // bool success = false;
//         // if ( parent != null ) {
//         //     success = parent.DeleteChildNode( this );
//         // }
//         // if ( success ) {
//         //     Debug.Log("The child node was successfully removed."  );
//         // }
//     }
//
//     public bool DeleteChildNode( iAINode node ) {
//         // Not possible to delete the child node of an AINode. Maybe this should be true?
//         return false;
//     }
// }
