using UnityEngine;
using System.Collections.Generic;
using NaughtyAttributes;

namespace XnTools.XnAI {
    /// <summary>
    /// <para>The orders work as follows:</para>
    /// <para>   - sequence - (AND) Runs in order UNTIL one child fails.
    ///              Returns failed if any one child fails</para>
    /// <para>   - selection - (OR) Runs in order UNTIL one child succeeds.
    ///              Returns success if any one child succeeds</para>
    /// <para>   - parallel - Runs ALL children regardless of returned eStatus.
    ///              Returns success if any one child succeeds</para>
    /// </summary>
    [System.Serializable]
    public enum eCompositeType { sequence, selection, parallel };

    /// <summary>
    /// <para>The orders work as follows:</para>
    /// <para>   - assigned - Will follow the order of children in childList</para>
    /// <para>   - utility - Will test utility and go in the order from highest to lowest</para>
    /// <para>   - random - Will make a random choice and stick with it while eStatus == busy</para>
    /// </summary>
    [System.Serializable]
    public enum eCompositeOrder { assigned, utility, random };
    

    [System.Serializable]
    public class AINode_Composite : iAINode {
        // public string _nodeName = iAINode.nodeNameNotSet;
        // public override string nodeName {
        //     get { return _nodeName; }
        //     set { if (_nodeName == iAINode.nodeNameNotSet) _nodeName = value; }
        // }

#region Node Selection & SetAITreeAndParent
        // protected bool isPlaying => Application.isPlaying;


        // [AllowNesting] [OnValueChanged( "SelectMe" )][HideIf( "isPlaying" )][Label("GetSelectNodeText")]
        // public bool thisNodeSelected = false;
        // protected override void SelectMe() {
        //     if (!IsThisNodeSelected()) {
        //         thisNodeSelected = aiTree.SelectNode( this );
        //     } else {
        //         thisNodeSelected = false;
        //         aiTree.DeselectNode( this );
        //     }
        // }
        //
        // protected override string GetSelectNodeText() {
        //     return ( IsThisNodeSelected() ) ? iAINode.nodeDeselectText : iAINode.nodeSelectText;
        // }
        //
        // public override void UpdateThisNodeSelected() {
        //     thisNodeSelected = IsThisNodeSelected();
        // }
        //
        // protected override bool IsThisNodeSelected() {
        //     if ( aiTree == null ) return false;
        //     iAINode selectedNode = aiTree.GetSelectedNode();
        //     if ( selectedNode == null ) return false;
        //     if ( selectedNode != this ) return false;
        //     return true;
        // }

        public override void SetAITreeAndParent( AITree tTree, iAINode tParent ) {
            base.SetAITreeAndParent(tTree, tParent);
            foreach ( iAINode node in childList ) {
                node.SetAITreeAndParent( tTree, this );
            }
        }

#endregion
        
        [AllowNesting][SerializeField]//[ShowIf( "isPlaying" )]
        public eStatus _status = eStatus.failed;
        public override eStatus status { get => _status; protected set => _status = value; }

        [AllowNesting][SerializeField]//[ShowIf( "isPlaying" )]
        public float _utility = 0;
        public override aFloat utility { get => _utility; protected set => _utility = value._value; }
        

        // [HideInInspector][SerializeField]
        // protected AITree aiTree;
        // public override AITree GetAITree() { return aiTree; }
        //
        // [HideInInspector][SerializeField]
        // protected iAINode parent;
        // public override iAINode GetParent() { return parent; }
        
        public eCompositeType  compositeType  = eCompositeType.sequence;
        public eCompositeOrder compositeOrder = eCompositeOrder.assigned;
        [AllowNesting]
        [SerializeField, SerializeReference]
        public List<iAINode> childList = new List<iAINode>();
        [AllowNesting]
        [ShowIf( "isPlaying" )]
        public int currentNode = iAINode.noNode;
        
        
#region Runtime Methods

        // Note: utilityOrder is also used for Random order
        [AllowNesting]
        [ShowIf( "isPlaying" )]
        public List<UtilityInfo> utilityOrder = new List<UtilityInfo>();
        
        public override aFloat CheckUtility() {
            utilityOrder.Clear();
            iAINode node;
            for ( int i = 0; i < childList.Count; i++ ) {
                node = childList[i];
                node.CheckUtility();
                utilityOrder.Add( new UtilityInfo( node.utility, node.nodeName, i ) );
            }
            // Generate a list of nodes in Utility order
            utilityOrder.Sort( delegate( UtilityInfo a, UtilityInfo b ) {
                return UtilityInfo.Compare( a, b );
            } );
            // Now, we know the Utility order of the nodes
            // Return the top Utility that was found
            if ( utilityOrder.Count == 0 ) return 0;
            return utilityOrder[0].utility;
        }

        protected int     executeIndex = 0;
        protected eStatus executeStatus = eStatus.failed;

        protected void ResetExecution() {
            if ( compositeOrder == eCompositeOrder.random ) {
                // Randomly sort the child nodes
                utilityOrder.Clear();
                iAINode node;
                for ( int i = 0; i < childList.Count; i++ ) {
                    node = childList[i];
                    utilityOrder.Add( new UtilityInfo( Random.value, node.nodeName, i ) );
                }
                // Generate a list of nodes in Utility order
                utilityOrder.Sort( delegate( UtilityInfo a, UtilityInfo b ) { return UtilityInfo.Compare( a, b ); } );
            }
            if ( compositeOrder == eCompositeOrder.utility ) {
                // Randomly sort the child nodes
                utilityOrder.Clear();
                iAINode node;
                for ( int i = 0; i < childList.Count; i++ ) {
                    node = childList[i];
                    aFloat utilValue = childList[i].CheckUtility();
                    utilityOrder.Add( new UtilityInfo( utilValue, node.nodeName, i ) );
                }
                // Generate a list of nodes in Utility order
                utilityOrder.Sort( delegate( UtilityInfo a, UtilityInfo b ) { return UtilityInfo.Compare( a, b ); } );
            }
            
            executeIndex = 0;
        }

        protected iAINode NextExecutionNode() { 
            // Return null when there is not a next node
            if ( executeIndex >= childList.Count ) return null;

            iAINode executeNode = null;
            
            switch ( compositeOrder ) {
            case eCompositeOrder.assigned:
                executeNode = childList[executeIndex];
                break;

            case eCompositeOrder.utility:
            case eCompositeOrder.random:
                executeNode = childList[utilityOrder[executeIndex].nodeIndex];
                break;
    
            }
            executeIndex++;
            
            return executeNode;
        }

        public override eStatus Execute(ref List<string> treeStatList, int callDepth) {
            int tSLCount = treeStatList.Count;
            ResetExecution();
            eStatus parallelStatus = eStatus.failed;
            bool executeNodes = true;
            for ( int i = 0; i < childList.Count; i++ ) {
                iAINode nextNode = NextExecutionNode();
                if ( nextNode == null ) break;
                if ( !executeNodes ) {
                    treeStatList.Add(nextNode.GetNodeStatusWithoutExecuting(callDepth+1)  );
                    continue;
                }
                status = nextNode.Execute(ref treeStatList, callDepth+1);
                switch ( compositeType ) {
                case eCompositeType.sequence:
                    // sequence - (AND) Runs in order UNTIL one child fails.
                    //             Returns failed if any one child fails
                    if ( status == eStatus.failed ) executeNodes = false;
                    if ( status == eStatus.busy ) executeNodes = false;
                    break;

                case eCompositeType.selection:
                    // selection - (OR) Runs in order UNTIL one child succeeds.
                    //              Returns success if any one child succeeds
                    if ( status == eStatus.success ) executeNodes = false;
                    if ( status == eStatus.busy ) executeNodes = false;
                    break;

                case eCompositeType.parallel:
                    // parallel - Runs ALL children regardless of returned eStatus.
                    //             Returns busy if any one child is busy
                    //             Or returns success if any one child succeeds
                    if ( parallelStatus == eStatus.failed && status == eStatus.success ) parallelStatus = status;
                    if ( status == eStatus.busy ) parallelStatus = status;
                    break;
                }
            }
            if ( compositeType == eCompositeType.parallel ) status = parallelStatus;
            
            treeStatList.Insert(tSLCount, GenerateStatus(callDepth));
            return status;
        }

#endregion

        public iAINode GetChild( int ndx=0 ) {
            if ( childList == null ) return null;

            if ( childList.Count - 1 < ndx ) return null;

            return childList[ndx];
        }

        public override void DeleteSelf() {
            bool success = false;
            if ( parent != null ) {
                success = parent.DeleteChildNode( this, GetChild( 0 ) );
            }
            if ( success )
                Debug.Log( $"The node {nodeName} was successfully removed." );
            else
                Debug.LogError( $"The node {nodeName} FAILED to be removed." );
        }

        public override bool DeleteChildNode( iAINode node = null, iAINode replacement=null ) {
            if ( node == null ) return false;
            
            if ( replacement != null ) {
                int ndx = childList.IndexOf( node );
                if ( ndx != -1 ) {
                    childList[ndx] = replacement;
                    SetAITreeAndParent( aiTree, parent );
                    return true;
                } else {
                    // ndx will be -1 in the case that node == null
                    return false;
                }
            }
            // In the case that there is no replacement, just remove the node 
            return childList.Remove( node );
            // No need to update the AITree and Parent of children, because all we did was remove an element.
        }



        public override bool ReplaceChildNode( iAINode oldNode, iAINode newNode ) {
            int ndx = childList.IndexOf( oldNode );
            if ( ndx != -1 ) {
                childList[ndx] = newNode;
                return true;
            }
            return false;
        }

        public override bool AddChildNode( iAINode node ) {
            if ( node == null ) return false;
            childList.Add( node );
            return true;
        }
        
    }
  
    
}
