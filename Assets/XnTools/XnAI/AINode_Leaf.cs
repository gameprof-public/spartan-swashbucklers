using UnityEngine;
using System.Collections.Generic;
using NaughtyAttributes;

namespace XnTools.XnAI {
    [System.Serializable]
    public class AINode_Leaf : iAINode {
        
        
        // public string _nodeName = iAINode.nodeNameNotSet;
        // public override string nodeName {
        //     get { return _nodeName; }
        //     set { if (_nodeName == iAINode.nodeNameNotSet) _nodeName = value; }
        // }

#region Node Selection & SetAITreeAndParent

        // [AllowNesting] [OnValueChanged( "SelectMe" )][HideIf( "isPlaying" )][Label("GetSelectNodeText")]
        // public bool thisNodeSelected = false;
        // protected void SelectMe() {
        //     if (!IsThisNodeSelected()) {
        //         thisNodeSelected = aiTree.SelectNode( this );
        //     } else {
        //         thisNodeSelected = false;
        //         aiTree.DeselectNode( this );
        //     }
        // }
        //
        // protected string GetSelectNodeText() {
        //     return ( IsThisNodeSelected() ) ? iAINode.nodeDeselectText : iAINode.nodeSelectText;
        // }
        //
        // public void UpdateThisNodeSelected() {
        //     thisNodeSelected = IsThisNodeSelected();
        // }
        //
        // protected bool IsThisNodeSelected() {
        //     if ( aiTree == null ) return false;
        //     iAINode selectedNode = aiTree.GetSelectedNode();
        //     if ( selectedNode == null ) return false;
        //     if ( selectedNode != this ) return false;
        //     return true;
        // }

        public override void SetAITreeAndParent( AITree tTree, iAINode tParent ) {
            base.SetAITreeAndParent(tTree, tParent);
        }
        
#endregion
        
        // [HideInInspector]
        // protected AITree aiTree;
        // public override AITree GetAITree() => aiTree;
        //
        // [HideInInspector]
        // protected iAINode parent;
        // public override iAINode GetParent() => parent;

        // protected bool isPlaying => Application.isPlaying;
        

        //[AllowNesting][SerializeField]//[ShowIf( "isPlaying" )]
        private eStatus _status = eStatus.failed;
        public override eStatus status { get => _status; protected set => _status = value; }

        //[AllowNesting][SerializeField]//[ShowIf( "isPlaying" )]
        private float _utility = 0;
        public override aFloat utility { get => _utility; protected set => _utility = value._value; }


        
#region iAINode Default Method Call Wrappers or Replacements

        // public override string ToString() {
        //     return ( this as iAINode ).ToString();
        // }

        // public bool AddDecoratorParent() {
        //     iAINode parentNode = this.GetParent();
        //     if ( parentNode == null ) {
        //         Debug.LogError( "For some reason, the parent node was null." );
        //         return false;
        //     }
        //     AINode_Decorator deco = new AINode_Decorator();
        //     parentNode.ReplaceChildNode( this, deco );
        //     ( (iAINode) deco ).AddChildNode( this );
        //     return true;
        // }

#endregion

#region Runtime Methods
        public override aFloat CheckUtility() {
            utilityChecked = true;
            aiTree.currentNode = this;
            utility = aiTree.InvokeCheckUtility( utilityMethod );
            aiTree.currentNode = null;
            return utility;
        }

        public override eStatus Execute(ref List<string> treeStatList, int callDepth) {
            if ( aiTree == null ) {
                Debug.LogError("AINode_Leaf.Execute called, but its aiTree was null!");
                status = eStatus.failed;
            } else if ( executeMethod == nullMethodName ) {
                status = eStatus.failed;
            } else {
                aiTree.currentNode = this;
                status = aiTree.InvokeExecute(executeMethod);
                aiTree.currentNode = null;
            }
            treeStatList.Add(GenerateStatus(callDepth));
            return status;
        }

#endregion



        // None of these three should do anything in a Leaf node
        public override void AddLeaf() { }
        public override void AddComposite() { }
        public override void AddDecorator() { }


        public override void DeleteSelf() {
            bool success = false;
            if ( parent != null ) {
                success = parent.DeleteChildNode( this, null );
            }
        
            if ( success )
                Debug.Log( $"The node {_nodeName} was successfully removed." );
            else
                Debug.LogError( $"The node {_nodeName} FAILED to be removed." );
        }


        public override bool DeleteChildNode( iAINode node = null, iAINode replacement=null ) {
            // Not possible to delete the child node of a Leaf. Maybe this should be true?
            Debug.LogError( $"It is NOT POSSIBLE to remove the child of node {_nodeName}" +
                       $" because {_nodeName} is an AINode_Leaf!" ); 
            return false;
        }
        
        public override bool ReplaceChildNode( iAINode oldNode, iAINode newNode ) {
            Debug.LogError( $"It is NOT POSSIBLE to replace a child of node {_nodeName}" +
                            $" because {_nodeName} is an AINode_Leaf!" ); 
            return false;
        }

        public override bool AddChildNode( iAINode node ) {
            Debug.LogError( $"It is NOT POSSIBLE to add a child to node {_nodeName}" +
                            $" because {_nodeName} is an AINode_Leaf!" ); 
            return true;
        }
        


        bool IsParentCompositeUtility() {
            if ( parent == null ) return false;
            iAINode tempParent = parent;
            // Read up through any Decorator parents
            while ( tempParent is AINode_Decorator ) {
                tempParent = tempParent.GetParent();
            } 
            if ( tempParent == null ) return false;
            
            AINode_Composite comp = tempParent as AINode_Composite;
            if ( comp == null ) return false;
            return ( comp.compositeOrder == eCompositeOrder.utility );
        }
        
        
        [Dropdown( "GetUtilityMethods" )][AllowNesting][ShowIf("IsParentCompositeUtility")]
        // [OnValueChanged("AssignUtilityMethod")]
        public string utilityMethod = iAINode.nullMethodName;
        
        [Dropdown( "GetExecuteMethods" )][AllowNesting]
        // [OnValueChanged("AssignExecuteMethod")]
        public string executeMethod = iAINode.nullMethodName;
        

        protected List<string> GetUtilityMethods() {
            return aiTree.utilityMethods;
            // return iAINode.GetMethodsByReturnType( this.GetType(), typeof(aFloat), "CheckUtility" );
        }
        protected List<string> GetExecuteMethods() {
            return aiTree.executeMethods;
            // return iAINode.GetMethodsByReturnType( this.GetType(), typeof(eStatus), "Execute" );
        }

    }
}
