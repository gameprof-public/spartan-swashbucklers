using UnityEngine;
using System.Collections.Generic;
using NaughtyAttributes;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace XnTools.XnAI {
    [System.Serializable]
    public class UtilityInfo {
        public string info = "No valid info";
        
        [HideInInspector] public aFloat utility;
        [HideInInspector] public string nodeName;
        [HideInInspector] public int    nodeIndex;

        public UtilityInfo() {
            info = "No valid info";
        }
        
        public UtilityInfo( aFloat _u, string _nn, int _nI ) {
            utility = _u;
            nodeName = _nn;
            nodeIndex = _nI;
            info = GenerateString();
        }

        public string GenerateString() {
            return $"{((float)utility):0.00} - [{nodeIndex:0}] {nodeName}";
        }
        
        public override string ToString() {
            return GenerateString();
        }

        static public int Compare( UtilityInfo a, UtilityInfo b ) {
            if ( a.utility > b.utility ) return -1;
            if ( b.utility > a.utility ) return 1;
            return 0;
        }
    }    
    
    
// #if UNITY_EDITOR
//     //TODO: Figure out why the CustomPropertyDrawer for UtilityInfo isn't working.
//     // https://github.com/dbrizov/NaughtyAttributes/issues/142 - Issue on NaughtyAttributes
//     // https://forum.unity.com/threads/solved-custompropertydrawer-not-being-using-in-editorgui-propertyfield.534968/
//     [CustomPropertyDrawer(typeof(UtilityInfo))]
//     public class UtilityInfo_Drawer : PropertyDrawer {
//         public override void OnGUI(Rect _, SerializedProperty property, GUIContent __) {
//             var childDepth = property.depth + 1;
//             PropertyDrawer pDraw = XnTools.PropertyDrawerFinder.FindDrawerForProperty( property );
//             if ( pDraw != null ) {
//                 pDraw.OnGUI(_, property, __);
//                 return;
//             } else {
//                 EditorGUILayout.PropertyField( property, includeChildren: false );
//             }
//
//             // PropertyDrawerFinder.Find(<SerializedProperty>).OnGUI() )
//             if (!property.isExpanded) {
//                 return;
//             }
//             EditorGUI.indentLevel++;
//             foreach (SerializedProperty child in property) {
//                 if (child.depth == childDepth && NaughtyAttributes.Editor.PropertyUtility.IsVisible(child)) {
//                     EditorGUILayout.PropertyField(child);
//                 }
//             }
//             EditorGUI.indentLevel--;
//         }
//
//         public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
//             PropertyDrawer pDraw = XnTools.PropertyDrawerFinder.FindDrawerForProperty( property );
//             if ( pDraw != null ) {
//                 return pDraw.GetPropertyHeight(property, label)
//                 - EditorGUIUtility.singleLineHeight - EditorGUIUtility.standardVerticalSpacing;
//             } else {
//                 return EditorGUI.GetPropertyHeight( property, label, includeChildren: false )
//                        - EditorGUIUtility.singleLineHeight - EditorGUIUtility.standardVerticalSpacing;
//             }
//         }
//     }
// #endif
    
}
