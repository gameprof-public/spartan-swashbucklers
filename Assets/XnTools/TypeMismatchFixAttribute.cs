using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif 

/// <summary>
/// This attribute fixes the Type Mismatch issue when you try to show a reference
/// to a Scene object in the Inspector of an Asset like a ScriptableObject.
/// From: https://www.reddit.com/r/Unity3D/comments/189qpk3/anyone_know_how_to_fix_type_mismatch_in/
/// </summary>
public class TypeMismatchFixAttribute : PropertyAttribute { }

#if UNITY_EDITOR
[CustomPropertyDrawer( typeof(TypeMismatchFixAttribute) )]
public class TypeMismatchFixDrawer : PropertyDrawer {
    public override void OnGUI( Rect position, SerializedProperty property, GUIContent label ) {
        EditorGUI.BeginProperty( position, label, property );

        // EditorGUI.LabelField(position, label);
        EditorGUI.ObjectField( position, label, property.objectReferenceValue, typeof(Object), true );

        EditorGUI.EndProperty();
    }
}
#endif